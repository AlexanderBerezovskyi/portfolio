//
//  ABAddPostTVC.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 05.09.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABAddPostTVC : UITableViewController

@property (strong, nonatomic) NSString *ownerID;

@end
