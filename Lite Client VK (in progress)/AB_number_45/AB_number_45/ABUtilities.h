//
//  ABUtilities.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 01.08.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ABUtilities : NSObject

+ (UIImage*)resizeImage:(UIImage *)image imageSize:(CGSize)size;
+ (NSInteger) getHeightForText:(NSString *)postText withWidth:(NSInteger)contentWidth;
+ (NSDate *) dateOfBirthBetween:(NSInteger)firstYearOld and:(NSInteger)secondYearOld;

@end
