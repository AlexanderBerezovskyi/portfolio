//
//  ABGroup.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 13.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABGroup : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSURL *avatar100URL;
@property (strong, nonatomic) NSString *groupID;

- (instancetype)initWithServerResponseGroupGet:(NSDictionary *) responseObject;

@end
