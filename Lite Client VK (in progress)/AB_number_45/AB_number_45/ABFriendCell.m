//
//  ABFriendCell.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 11.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABFriendCell.h"

@implementation ABFriendCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
