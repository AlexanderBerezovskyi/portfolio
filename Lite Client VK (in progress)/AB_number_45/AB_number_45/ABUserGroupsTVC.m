//
//  ABUserGroupsTVC.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 13.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABUserGroupsTVC.h"
#import "ABServerManager.h"
#import "ABGroup.h"
#import "UIImageView+AFNetworking.h"
#import "ABUserWallTVC.h"

static NSString *showGroupWallIdentifier = @"showGroupWall";

static NSInteger groupsInRequest = 20;

@interface ABUserGroupsTVC () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *groupsArray;
@property (strong, nonatomic) NSString *groupID;

@end

@implementation ABUserGroupsTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // set navigation title
    self.navigationItem.title = @"Groups";
    
    self.groupsArray = [NSMutableArray array];
        
    [self getGroups];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - API

- (void) getGroups {
    
    [[ABServerManager sharedManager]
     getUserSubscriptionsGroupsByUserID:self.userID
     withOffset:self.groupsArray.count
     count:groupsInRequest
     onSuccess:^(NSArray *subscriptionsGroups) {
                  
         [self.groupsArray addObjectsFromArray:subscriptionsGroups];
         
         NSMutableArray *newPaths = [NSMutableArray array];
         
         for (int i = (int)self.groupsArray.count - (int)subscriptionsGroups.count;
              i < self.groupsArray.count; i++) {
             
             [newPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
         }
         
         [self.tableView beginUpdates];
         
         [self.tableView insertRowsAtIndexPaths:newPaths
                               withRowAnimation:UITableViewRowAnimationLeft];
         
         [self.tableView endUpdates];
         
     } onFailure:^(NSError *error, NSInteger statusCode) {
         NSLog(@"error = %@, status code = %li", [error localizedDescription], statusCode);
     }];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.groupsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *groupCellIdentifier = @"GroupCell";
    
    UITableViewCell *groupCell = [tableView dequeueReusableCellWithIdentifier:groupCellIdentifier];
    
    groupCell.detailTextLabel.text = @"";
    
    if (!groupCell) {
        groupCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                              reuseIdentifier:groupCellIdentifier];
    }
    
    if (indexPath.row == (self.groupsArray.count - 1)) {
        [self getGroups];
    }
    
    ABGroup *group = [self.groupsArray objectAtIndex:indexPath.row];
    
    // name
    groupCell.textLabel.text = [NSString stringWithFormat:@"%@", group.name];
    
    // avatar
    NSURLRequest *request = [NSURLRequest requestWithURL:group.avatar100URL];
    
    __weak UITableViewCell *weakGroupCell = groupCell;
    
    groupCell.imageView.image = nil;
    
    [groupCell.imageView
     setImageWithURLRequest:request
     placeholderImage:nil
     success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
         
         [UIView animateWithDuration:0.5f
                               delay:0.f
                             options:UIViewAnimationOptionTransitionFlipFromBottom
                          animations:^{
                              
                              weakGroupCell.imageView.image = image;
                              [weakGroupCell layoutSubviews];
                              
                          }
                          completion:^(BOOL finished) {
                          }];
     }
     failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
         NSLog(@"error = %@", [error localizedDescription]);
     }];
    
    groupCell.detailTextLabel.textColor = [UIColor brownColor];
    
    groupCell.detailTextLabel.text = [NSString stringWithFormat:@"%@", group.status];
    
    return groupCell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ABGroup *group = [self.groupsArray objectAtIndex:indexPath.row];
    
    self.groupID = [NSString stringWithFormat:@"-%@", group.groupID];
    
    [self performSegueWithIdentifier:showGroupWallIdentifier sender:nil];
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:showGroupWallIdentifier]){
        ABUserWallTVC *userWallTVC = (ABUserWallTVC *)segue.destinationViewController;
        userWallTVC.userID = self.groupID;
    }
}




@end
