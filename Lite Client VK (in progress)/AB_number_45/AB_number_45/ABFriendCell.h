//
//  ABFriendCell.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 11.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABFriendCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *onlineLabel;

@end
