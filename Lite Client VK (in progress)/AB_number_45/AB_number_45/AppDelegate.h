//
//  AppDelegate.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 06.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

