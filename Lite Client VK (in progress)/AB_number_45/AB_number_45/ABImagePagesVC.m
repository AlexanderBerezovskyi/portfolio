//
//  ABImagePagesVC.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 09.08.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABImagePagesVC.h"
#import "UIImageView+AFNetworking.h"

@interface ABImagePagesVC ()

@end

@implementation ABImagePagesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURLRequest *requestItemImage = [NSURLRequest requestWithURL:[NSURL URLWithString:self.imageStringURL]];
    
    self.itemImageView.image = nil;
    
    [self.itemImageView
     setImageWithURLRequest:requestItemImage
     placeholderImage:nil
     success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                              
         self.itemImageView.image = image;

     }
     failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
         NSLog(@"error = %@", [error localizedDescription]);
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Actions

- (IBAction)goToUserWallAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}





@end
