//
//  ABMessage.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 06.09.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABMessage.h"

@implementation ABMessage

- (instancetype)initWithServerResponseMessageGet:(NSDictionary *) responseObject {
    
    self = [super init];
    if (self) {
        
        // text (body message)
        self.bodyText = [responseObject objectForKey:@"body"];
        
        // message owner
        self.messageFromID = [responseObject objectForKey:@"from_id"];
        
        // message date
        NSInteger globalCreatedSeconds = [[responseObject objectForKey:@"date"] integerValue];
        NSDate *globalCreatedDate = [NSDate dateWithTimeIntervalSince1970:globalCreatedSeconds];
        self.messageSendDate = globalCreatedDate;
        
    }
    return self;
}

@end
