//
//  ABUtilities.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 01.08.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABUtilities.h"

@implementation ABUtilities

#pragma mark - Resize image

+ (UIImage*)resizeImage:(UIImage *)image imageSize:(CGSize)size {
    
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0,0,size.width,size.height)];
    
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    //here is the scaled image which has been changed to the size specified
    UIGraphicsEndImageContext();
    return newImage;
}


#pragma mark - Height of text

+ (NSInteger) getHeightForText:(NSString *)postText withWidth:(NSInteger)contentWidth {
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:17]};
    
    CGRect rect = [postText boundingRectWithSize:CGSizeMake(contentWidth, CGFLOAT_MAX)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:attributes
                                         context:nil];
    
    return rect.size.height;
}


#pragma mark - Get random birth date between two flags

+ (NSDate *) dateOfBirthBetween:(NSInteger)firstYearOld and:(NSInteger)secondYearOld {
    
    NSDate *currentDate = [NSDate date];
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    
    NSDateComponents *componentsFromCurrentDate = [currentCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:currentDate];
    
    
    [componentsFromCurrentDate setYear:[componentsFromCurrentDate year] - secondYearOld];
    NSDate *dateSecondYearsAgo = [currentCalendar dateFromComponents:componentsFromCurrentDate];
    
    
    [componentsFromCurrentDate setYear:[componentsFromCurrentDate year] + secondYearOld - firstYearOld];
    NSDate *dateFirstYearsAgo = [currentCalendar dateFromComponents:componentsFromCurrentDate];
    
    
    NSDateComponents *componentsBetweenFirstAndSecontYearsOld = [currentCalendar components:NSCalendarUnitSecond fromDate:dateSecondYearsAgo toDate:dateFirstYearsAgo options:NO];
    
    NSInteger betweenFirstAndSecond = [componentsBetweenFirstAndSecontYearsOld second];
    
    
    NSDate *dateOfBirth = [NSDate dateWithTimeInterval:arc4random() % betweenFirstAndSecond
                                             sinceDate:dateSecondYearsAgo];
    
    return dateOfBirth;
}

@end
