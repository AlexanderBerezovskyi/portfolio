//
//  ABMessagesTVC.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 06.09.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABMessagesTVC.h"
#import "ABServerManager.h"

static NSInteger messagesInRequest = 20;

@interface ABMessagesTVC () <UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray *messagesArray;

@end

@implementation ABMessagesTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.messagesArray = [NSMutableArray array];
    
    [self getMessages];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - API

- (void) getMessages {
    
    [[ABServerManager sharedManager]
     getMessagesToUserID:self.userID
     withOffset:self.messagesArray.count
     count:messagesInRequest
     onSuccess:^(NSArray *messages) {
         
         [self.messagesArray addObjectsFromArray:messages];
         
         NSMutableArray *newPaths = [NSMutableArray array];
         
         for (int i = (int)self.messagesArray.count - (int)messages.count;
              i < self.messagesArray.count; i++) {
             
             [newPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
         }
         
         [self.tableView beginUpdates];
         
         [self.tableView insertRowsAtIndexPaths:newPaths
                               withRowAnimation:UITableViewRowAnimationRight];
         
         [self.tableView endUpdates];
         
     } onFailure:^(NSError *error, NSInteger statusCode) {
         NSLog(@"error = %@, status code = %li", [error localizedDescription], statusCode);
     }];

}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messagesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *messageCellIdentifier = @"MessageCell";
    
    UITableViewCell *messageCell = [tableView dequeueReusableCellWithIdentifier:messageCellIdentifier];
    
    if (!messageCell) {
        messageCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:messageCellIdentifier];
    }
    
    return messageCell;
}

@end
