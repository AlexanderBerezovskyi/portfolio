//
//  ABOrientationNVC.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 21.08.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABOrientationNVC.h"
#import "ABUserInfoTVC.h"

@interface ABOrientationNVC ()

@end

@implementation ABOrientationNVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
        id currentViewController = self.topViewController;
    
        if ([currentViewController isKindOfClass:[ABUserInfoTVC class]]) {
            return UIInterfaceOrientationMaskPortrait;
        }
    
    return UIInterfaceOrientationMaskAll;
}


@end
