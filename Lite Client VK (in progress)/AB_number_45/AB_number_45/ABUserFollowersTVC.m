//
//  ABUserFollowersTVC.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 13.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABUserFollowersTVC.h"
#import "ABServerManager.h"
#import "ABUser.h"
#import "UIImageView+AFNetworking.h"
#import "ABUserWallTVC.h"

static NSString *showUserWallIdentifier = @"showUserWall";

static NSInteger followersInRequest = 20;

@interface ABUserFollowersTVC () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *followersArray;
@property (strong, nonatomic) NSString *followerID;

@end

@implementation ABUserFollowersTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // set navigation title
    self.navigationItem.title = @"Followers";
    
    self.followersArray = [NSMutableArray array];
    
    [self getFollowers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - API

- (void) getFollowers {
    
    [[ABServerManager sharedManager]
     getUserFollowersWithID:self.userID
     withOffset:self.followersArray.count
     count:followersInRequest
     onSuccess:^(NSArray *followers) {
        
         [self.followersArray addObjectsFromArray:followers];
         
         NSMutableArray *newPaths = [NSMutableArray array];
         
         for (int i = (int)self.followersArray.count - (int)followers.count;
              i < self.followersArray.count; i++) {
             
             [newPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
         }
         
         [self.tableView beginUpdates];
         
         [self.tableView insertRowsAtIndexPaths:newPaths
                               withRowAnimation:UITableViewRowAnimationRight];
         
         [self.tableView endUpdates];
         
     } onFailure:^(NSError *error, NSInteger statusCode) {
         NSLog(@"error = %@, status code = %li", [error localizedDescription], statusCode);
     }];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.followersArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *followerCellIdentifier = @"FollowerCell";
    
    UITableViewCell *followerCell = [tableView dequeueReusableCellWithIdentifier:followerCellIdentifier];
    
    followerCell.detailTextLabel.text = @"";
    
    if (!followerCell) {
        followerCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                               reuseIdentifier:followerCellIdentifier];
    }
    
    if (indexPath.row == (self.followersArray.count - 1)) {
        [self getFollowers];
    }
    
    ABUser *follower = [self.followersArray objectAtIndex:indexPath.row];
    
    // name
    followerCell.textLabel.text = [NSString stringWithFormat:@"%@ %@", follower.firstName, follower.lastName];
    
    // avatar
    NSURLRequest *request = [NSURLRequest requestWithURL:follower.avatar100URL];
    
    __weak UITableViewCell *weakFollowerCell = followerCell;
    
    followerCell.imageView.image = nil;
    
    [followerCell.imageView
     setImageWithURLRequest:request
     placeholderImage:nil
     success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
         
         [UIView animateWithDuration:0.5f
                               delay:0.f
                             options:UIViewAnimationOptionTransitionCurlDown
                          animations:^{
                              
                              weakFollowerCell.imageView.image = image;
                              [weakFollowerCell layoutSubviews];
                              
                          }
                          completion:^(BOOL finished) {
                          }];
     }
     failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
         NSLog(@"error = %@", [error localizedDescription]);
     }];
    
    followerCell.detailTextLabel.textColor = [UIColor blueColor];
    
    if (follower.skype) {
        followerCell.detailTextLabel.text = [NSString stringWithFormat:@"skype: %@", follower.skype];
    } else {
        followerCell.detailTextLabel.text = @"";
    }
    
    return followerCell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ABUser *user = [self.followersArray objectAtIndex:indexPath.row];
    
    self.followerID = user.friendID;
        
    [self performSegueWithIdentifier:showUserWallIdentifier sender:nil];
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:showUserWallIdentifier]){
        ABUserWallTVC *userWallTVC = (ABUserWallTVC *)segue.destinationViewController;
        userWallTVC.userID = self.followerID;
    }
}





@end
