//
//  ABAccessToken.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 24.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABAccessToken : NSObject

@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSDate *expirationDate;
@property (nonatomic, strong) NSString *userID;

@end
