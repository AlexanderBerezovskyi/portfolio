//
//  ABUserInfoTVC.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 11.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABUserInfoTVC.h"
#import "ABServerManager.h"
#import "UIImageView+AFNetworking.h"
#import "ABUser.h"
#import "UICountingLabel.h"
#import "ABUserFollowersTVC.h"
#import "ABUserGroupsTVC.h"
#import "ABUserWallTVC.h"
#import "ABMessagesTVC.h"

static NSString *showFollowersIdentifier = @"showFollowers";
static NSString *showSubscriptionsGroupsIdentifier = @"showSubscriptionsGroups";
static NSString *showWallIdentifier = @"showWall";
static NSString *showMessagesIdentifier = @"showMessages";

typedef enum {
    
    ABCellIndexZero,
    ABCellIndexOne
    
} ABCellIndex;

@interface ABUserInfoTVC () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *bigAvatarImageView;

@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *birthDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *onlineLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastSeenDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *subscriptionsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *followersCountLabel;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sentMessageButton;


@property (strong, nonatomic) ABUser *user;

@end

@implementation ABUserInfoTVC


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self getUserInfo];
    
    [[ABServerManager sharedManager]
     getUserSubscriptionsCountWithID:self.userID
     onSuccess:^(NSInteger subscriptionsCount) {
        
    [self countingLabel:self.subscriptionsCountLabel from:0.f to:subscriptionsCount];
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, status code = %li", [error localizedDescription], statusCode);
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - API

- (void) getUserInfo {
    
    [[ABServerManager sharedManager]
     getUserInfoWithID:self.userID
     onSuccess:^(ABUser *user) {
         
         self.user = user;
         
         // set navigation title
         self.navigationItem.title = [NSString stringWithFormat:@"%@ %@", self.user.firstName, self.user.lastName];
         
         [self.bigAvatarImageView setImageWithURL:user.avatarMaxSize];

         self.fullNameLabel.text = [NSString stringWithFormat:@"%@ %@", self.user.firstName, self.user.lastName];
         
         if (user.dateOfBirth) {
             
             NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
             [dateFormat setDateFormat:@"dd MMMM yyyy"];
             self.birthDateLabel.text = [NSString stringWithFormat:@"%@", [dateFormat stringFromDate:user.dateOfBirth]];
         }

         if (user.city) {
             
             [[ABServerManager sharedManager] getCityNameByID:user.city onSuccess:^(NSString *cityName) {
                
                 self.cityLabel.text = [NSString stringWithFormat:@"%@", cityName];
                 
             } onFailure:^(NSError *error, NSInteger statusCode) {
                 NSLog(@"error = %@, status code = %li", [error localizedDescription], statusCode);
             }];
         }
         
         if (user.online) {
             self.onlineLabel.text = @"Online";
             self.onlineLabel.textColor = [UIColor greenColor];
         } else {
             self.onlineLabel.text = @"Offline";
             self.onlineLabel.textColor = [UIColor redColor];
         }
         
         if ((user.lastSeenDate) && (!user.online)){
             
             NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
             [dateFormat setDateFormat:@"dd MMMM yyyy HH:mm"];
             
             [user.lastSeenDate descriptionWithLocale:[NSLocale systemLocale]];
             
             self.lastSeenDateLabel.text = [NSString stringWithFormat:@"was here %@", [dateFormat stringFromDate:user.lastSeenDate]];
         }
         
         if (user.followersCount) {
             
             [self countingLabel:self.followersCountLabel from:0.f to:user.followersCount];
         }

         [self.tableView reloadInputViews];
         
     }
     onFailure:^(NSError *error, NSInteger statusCode) {
         NSLog(@"error = %@, status code = %li", [error localizedDescription], statusCode);
     }];
}


#pragma mark - UITableViewDelegate

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 2) {
        return YES;
    }
    
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 2) {
        
        [self performSegueWithIdentifier:showWallIdentifier sender:nil];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        return self.tableView.bounds.size.width;
    } else if (indexPath.row == 1) {
        return 170.f;
    } else {
        return 60.f;
    }
}


#pragma mark - UICountingLabel

- (void) countingLabel:(UILabel *)label from:(CGFloat)firstValue to:(CGFloat)secondValue {
    
    UICountingLabel* countingLabel = [[UICountingLabel alloc] initWithFrame:label.frame];
    [[self.tableView.visibleCells objectAtIndex:ABCellIndexOne] addSubview:countingLabel];
    countingLabel.format = @"%1.f";
    countingLabel.method = UILabelCountingMethodEaseInOut;
    countingLabel.textColor = [UIColor whiteColor];
    countingLabel.textAlignment = NSTextAlignmentCenter;
    [countingLabel setFont:[UIFont fontWithName:@"Noteworthy-Bold" size:17.f]];
    
    [countingLabel countFrom:firstValue to:secondValue withDuration:1.5f];
}


#pragma mark - Actions

- (IBAction)followersAction:(UIButton *)sender {
    [self performSegueWithIdentifier:showFollowersIdentifier sender:nil];
}

- (IBAction)groupsAction:(UIButton *)sender {
    [self performSegueWithIdentifier:showSubscriptionsGroupsIdentifier sender:nil];
}

- (IBAction)sentMessageAction:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:showMessagesIdentifier sender:nil];
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:showFollowersIdentifier]){
        
        ABUserFollowersTVC *userFollowersTVC = (ABUserFollowersTVC *)segue.destinationViewController;
        userFollowersTVC.userID = self.userID;
        
    } else if ([segue.identifier isEqualToString:showSubscriptionsGroupsIdentifier]) {
        
        ABUserGroupsTVC *userGroupsTVC = (ABUserGroupsTVC *)segue.destinationViewController;
        userGroupsTVC.userID = self.userID;
    
    } else if ([segue.identifier isEqualToString:showWallIdentifier]) {
        
        ABUserWallTVC *userWallTVC = (ABUserWallTVC *)segue.destinationViewController;
        userWallTVC.userID = self.userID;
    
    } else if ([segue.identifier isEqualToString:showMessagesIdentifier]) {
        
        ABMessagesTVC *messagesTVC = (ABMessagesTVC *)segue.destinationViewController;
        
        messagesTVC.userID = self.userID;
    }
}









@end
