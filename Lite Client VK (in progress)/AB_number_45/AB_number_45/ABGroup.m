//
//  ABGroup.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 13.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABGroup.h"

@implementation ABGroup

- (instancetype)initWithServerResponseGroupGet:(NSDictionary *) responseObject {
    
    self = [super init];
    if (self) {
        
        // name
        self.name = [responseObject objectForKey:@"name"];
        
        // status
        self.status = [responseObject objectForKey:@"status"];
        
        // avatar100
        NSString *stringAvatar100URL = [responseObject objectForKey:@"photo_medium"];
        if (stringAvatar100URL) {
            self.avatar100URL = [NSURL URLWithString:stringAvatar100URL];
        }
        
        // id
        self.groupID = [responseObject objectForKey:@"gid"];
    }
    return self;
}

@end
