//
//  ABPageVC.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 09.08.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABPageVC.h"
#import "ABImagePagesVC.h"

@interface ABPageVC () <UIPageViewControllerDataSource>

@end

@implementation ABPageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self reloadInputViews];
    self.dataSource = self;
    
    ABImagePagesVC *initialVC = (ABImagePagesVC *)[self viewControllerAtIndex:self.itemIndexPath.row];
    NSArray *viewControllers = [NSArray arrayWithObject:initialVC];
    
    [self setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIPageViewControllerDataSource

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = ((ABImagePagesVC *) viewController).pageIndex;
    
    if (index == 0 || index == NSNotFound){
        return nil;
    }
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = ((ABImagePagesVC *) viewController).pageIndex;
    
    if (index == NSNotFound){
        return nil;
    }
    index++;
    
    if (index == self.itemsArray.count) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
}

#pragma mark - Reduce Metods

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    ABImagePagesVC *imagePagesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ABImagePagesVC"];
    
    imagePagesVC.imageStringURL = [self.itemsArray objectAtIndex:index];
    imagePagesVC.pageIndex = index;
    
    return imagePagesVC;
}

















@end
