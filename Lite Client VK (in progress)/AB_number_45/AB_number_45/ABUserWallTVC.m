//
//  ABUserWall.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 14.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABUserWallTVC.h"
#import "ABServerManager.h"
#import "ABWall.h"
#import "UIImageView+AFNetworking.h"
#import "ABWallCellNew.h"
#import "ABUtilities.h"
#import "ABPageVC.h"
#import "ABAddPostTVC.h"

static NSInteger postsInRequest = 5;
static NSString *wallCellNewIdentifier = @"WallCellNew";

static NSString *showPageVCIdentifier = @"showPageVC";
static NSString *showAddPostVCIdentifier = @"showAddPostVC";

@interface ABUserWallTVC () <UITableViewDataSource, UITableViewDelegate>

typedef void (^ItemBlock)(NSIndexPath*, NSArray*);
@property (copy, nonatomic) ItemBlock itemBlock;
    
@property (strong, nonatomic) NSMutableArray *postsArray;
@property (strong, nonatomic) NSArray *itemsArray;
@property (strong, nonatomic) NSIndexPath *itemIndexPath;
@property (strong, nonatomic) UIView *itemView;
@property (strong, nonatomic) NSString *wallPostID;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *addPostButton;


@end

@implementation ABUserWallTVC


- (void)viewDidLoad {
    [super viewDidLoad];

    [self.tableView registerNib:[UINib nibWithNibName:@"ABWallCellNew" bundle:nil]
         forCellReuseIdentifier:wallCellNewIdentifier];
    
    self.postsArray = [NSMutableArray array];
    
    [self getWall];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 45.f;
    
    __weak typeof(self) weakSelf = self;
    
    self.itemBlock = ^(NSIndexPath *selectedItemIndexPath, NSArray *itemsArray) {
        
        weakSelf.itemsArray = itemsArray;
        weakSelf.itemIndexPath = selectedItemIndexPath;
        
        [weakSelf performSegueWithIdentifier:showPageVCIdentifier sender:nil];
    };
    
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    [refresh addTarget:self
                action:@selector(refreshWall)
      forControlEvents:UIControlEventValueChanged];
    
    self.refreshControl = refresh;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self refreshWall];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}


#pragma mark - API

- (void) getWall {
    
    [[ABServerManager sharedManager]
     getWallPostsOfUserID:self.userID
     withOffset:self.postsArray.count
     count:postsInRequest
     
     onSuccess:^(NSArray *posts) {
                  
         [self.postsArray addObjectsFromArray:posts];
         
         NSMutableArray *newPaths = [NSMutableArray array];
         
         for (int i = (int)self.postsArray.count - (int)posts.count;
              i < self.postsArray.count; i++) {
             
             [newPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
         }
         
         [self.tableView beginUpdates];
         
         [self.tableView insertRowsAtIndexPaths:newPaths
                               withRowAnimation:UITableViewRowAnimationBottom];
         
         [self.tableView endUpdates];
         
     } onFailure:^(NSError *error, NSInteger statusCode) {
         NSLog(@"error = %@, status code = %li", [error localizedDescription], statusCode);
     }];
}


#pragma mark - Refresh Wall

- (void) refreshWall {
    
    [[ABServerManager sharedManager]
     getWallPostsOfUserID:self.userID
     withOffset:0
     count:MAX(postsInRequest, self.postsArray.count)
     
     onSuccess:^(NSArray *posts) {
         
         [self.postsArray removeAllObjects];
         [self.postsArray addObjectsFromArray:posts];
         [self.tableView reloadData];
         
         [self.refreshControl endRefreshing];
         
     } onFailure:^(NSError *error, NSInteger statusCode) {
         NSLog(@"error = %@, status code = %li", [error localizedDescription], statusCode);
         [self.refreshControl endRefreshing];
     }];
    
}



#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.postsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ABWallCellNew *wallCell = [tableView dequeueReusableCellWithIdentifier:wallCellNewIdentifier];
    ABWall *wall = [self.postsArray objectAtIndex:indexPath.row];
    
    wallCell.itemBlock = self.itemBlock;
    
    wallCell.contentWidth = self.tableView.contentSize.width - 20;
    wallCell.imagesCollectionViewArray = wall.postImagesArray;
    
    if (indexPath.row == (self.postsArray.count - 1)) {
        [self getWall];
    }
    
    // Post text
    wallCell.postMessageLabel.numberOfLines = 0;
    wallCell.postMessageLabel.text = wall.postText;
    
    // User photo
    NSURLRequest *requestUserPhoto = [NSURLRequest requestWithURL:wall.photoUser];
    
    __weak ABWallCellNew *weakWallCell = wallCell;
    
    wallCell.postAutorImageView.image = nil;
    
    [wallCell.postAutorImageView
     setImageWithURLRequest:requestUserPhoto
     placeholderImage:nil
     success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
         
         [UIView animateWithDuration:0.5f
                               delay:0.f
                             options:UIViewAnimationOptionTransitionCrossDissolve
                          animations:^{
                              
                              weakWallCell.postAutorImageView.image = image;
                              [weakWallCell layoutSubviews];
                          }
                          completion:^(BOOL finished) {
                              
                          }];
     }
     failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
         NSLog(@"error = %@", [error localizedDescription]);
     }];
    
    // User name
    wallCell.nameAuthorPostLabel.text = wall.nameAuthorPost;
    
    // Post date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale systemLocale]];
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm"];

    wallCell.postDateLabel.text = [dateFormatter stringFromDate:wall.postCreatedDate];
    
    // Likes count
    wallCell.likeCountLabel.text = [NSString stringWithFormat:@"%li", wall.likesCount];
    
    // Can make like (after wall download)
    if (wall.canMakeLike) {
        [wallCell.likeButton setImage:[UIImage imageNamed:@"heart_3.png"]
                             forState:UIControlStateNormal];
    } else {
        [wallCell.likeButton setImage:[UIImage imageNamed:@"heart_1.ico"]
                             forState:UIControlStateNormal];
    }
    
    [wallCell.likeButton addTarget:self action:@selector(checkButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    self.wallPostID = wall.wallPostID;
    
    
    // First image in post
    wallCell.postImageView.image = nil;
    
    if (wall.postPhotoBig) {
        
        NSURLRequest *requestBigPhoto = [NSURLRequest requestWithURL:wall.postPhotoBig];
        
        [wallCell.postImageView
         setImageWithURLRequest:requestBigPhoto
         placeholderImage:[UIImage imageNamed:@"download_5.png"]
         success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
             
             CGFloat scaleCoefficient = 1.f;
            
             if (image.size.width < weakWallCell.contentWidth) {
                 
                 scaleCoefficient = weakWallCell.contentWidth / image.size.width;
                 
                 CGSize postImageSize = CGSizeMake(image.size.width*scaleCoefficient,
                                                   image.size.height*scaleCoefficient);

                 UIImage *newImage = [ABUtilities resizeImage:image imageSize:postImageSize];
                 
                 weakWallCell.postImageView.image = newImage;
                 
             } else {
                 weakWallCell.postImageView.image = image;
             }
             
             UIView *currentView = self.view;
             
             if ((response != nil) && (currentView)) {
                 [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
             }
         }
         failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
             NSLog(@"error = %@", [error localizedDescription]);
         }];
    }

    // Many images in post
    wallCell.collectionViewHeight.constant = [wallCell getHeightCollectionViewWithItemsArray:wallCell.imagesCollectionViewArray andContentWidth:wallCell.contentWidth];
    
    // Can add post button
    (wall.canPost == YES) ?
    (self.addPostButton.enabled = YES) : (self.addPostButton.enabled = NO);

    wallCell.layer.shouldRasterize = YES;
    wallCell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    [wallCell.imagesCollectionView reloadData];
        
    return wallCell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - Actions

- (void)checkButtonTapped:(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    ABWall *wall = [self.postsArray objectAtIndex:indexPath.row];
    ABWallCellNew *wallCell = [self.tableView cellForRowAtIndexPath:indexPath];
        
    if ((indexPath != nil) && (wall.canMakeLike == YES)) {
        
        [[ABServerManager sharedManager]
         addLikeOnWallPostWithPostID:wall.wallPostID
         andOwnerID:self.userID
         onSuccess:^(NSUInteger likesCount) {
             
             wallCell.likeCountLabel.text = [NSString stringWithFormat:@"%lu", likesCount];
             wall.canMakeLike = NO;
             
         } onFailure:^(NSError *error, NSInteger statusCode) {
             NSLog(@"error = %@", [error localizedDescription]);
         }];
        
    } else if ((indexPath != nil) && (wall.canMakeLike == NO)) {
        
        [[ABServerManager sharedManager]
         deleteLikeOnWallPostWithPostID:wall.wallPostID
         andOwnerID:self.userID
         onSuccess:^(NSUInteger likesCount) {
             
             wallCell.likeCountLabel.text = [NSString stringWithFormat:@"%lu", likesCount];
             wall.canMakeLike = YES;
             
         } onFailure:^(NSError *error, NSInteger statusCode) {
             NSLog(@"error = %@", [error localizedDescription]);
         }];
    }
}

- (IBAction)addPostAction:(UIBarButtonItem *)sender {
    
    [self performSegueWithIdentifier:showAddPostVCIdentifier sender:nil];
}


#pragma mark - Segues

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:showPageVCIdentifier]){
        
        ABPageVC *pageTVC = (ABPageVC *)segue.destinationViewController;

        pageTVC.itemsArray = self.itemsArray;
        pageTVC.itemIndexPath = self.itemIndexPath;
        pageTVC.view.backgroundColor = [UIColor whiteColor];

    } else if ([segue.identifier isEqualToString:showAddPostVCIdentifier]) {
        
        ABAddPostTVC *addPostVC = (ABAddPostTVC *)segue.destinationViewController;
        addPostVC.ownerID = self.userID;
    }
}





@end
