//
//  ABAddPostTVC.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 05.09.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABAddPostTVC.h"
#import "ABServerManager.h"

@interface ABAddPostTVC () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *addPostTextView;
@property (weak, nonatomic) IBOutlet UIButton *sendTextButton;

@end

@implementation ABAddPostTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sendTextButton.enabled = NO;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    
    if (![textView.text isEqualToString:@""]) {
        self.sendTextButton.enabled = YES;
    } else {
        self.sendTextButton.enabled = NO;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text {
    // Any new character added is passed in as the "text" parameter
    if ([text isEqualToString:@"\n"]) {
        // Be sure to test for equality using the "isEqualToString" message
        [textView resignFirstResponder];
        
        // Return NO so that the final '\n' character doesn't get added
        return NO;
    }
    // For any other character return YES so that the text gets added to the view
    return YES;
}

#pragma mark - Actions

- (IBAction)sendPostAction:(UIButton *)sender {
    
    if (![self.addPostTextView.text isEqualToString:@""]) {
                
        [[ABServerManager sharedManager]
         addPostOnWallUserWithID:self.ownerID
         withText:self.addPostTextView.text
         onSuccess:^(NSString *postID) {
             
             [self dismissViewControllerAnimated:YES completion:nil];
             
         } onFailure:^(NSError *error, NSInteger statusCode) {
             NSLog(@"error = %@", [error localizedDescription]);
         }];
    }
}

- (IBAction)cancelAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}




@end
