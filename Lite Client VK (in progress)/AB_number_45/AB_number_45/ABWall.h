//
//  ABWall.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 25.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABWall : NSObject

@property (strong, nonatomic) NSString *nameAuthorPost;
@property (strong, nonatomic) NSURL *photoUser;
@property (strong, nonatomic) NSString *postText;
@property (strong, nonatomic) NSMutableArray *postImagesArray;
@property (strong, nonatomic) NSURL *postPhotoBig;
@property (assign, nonatomic) NSUInteger likesCount;
@property (assign, nonatomic) Boolean canMakeLike;
@property (strong, nonatomic) NSString *wallPostID;
@property (strong, nonatomic) NSDate *postCreatedDate;
@property (strong, nonatomic) NSString *ownerID;
@property (assign, nonatomic) Boolean canPost;

- (instancetype)initWithServerResponseWallPostsGet:(NSDictionary *) responseObject
                                     userWhoPosted:(NSDictionary *) userPosted
                                    groupWhoPosted:(NSDictionary *) groupPosted;


@end
