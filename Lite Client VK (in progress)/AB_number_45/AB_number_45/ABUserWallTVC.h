//
//  ABUserWall.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 14.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABUserWallTVC : UITableViewController

typedef void (^ReloadBlock)(void);

@property (strong, nonatomic) NSString *userID;

@end
