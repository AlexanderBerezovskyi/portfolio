    //
//  ABUser.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 07.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABUser.h"
#import <UIKit/UIKit.h>

@implementation ABUser

- (instancetype)initWithServerResponseUserGetAuthorize:(NSDictionary *)responseObject {
    
    self = [super init];
    if (self) {
        
        // authorize
        self.firstName = [responseObject objectForKey:@"first_name"];
        self.lastName = [responseObject objectForKey:@"last_name"];
        self.userID = [responseObject objectForKey:@"uid"];
    }
    return self;
}

- (instancetype)initWithServerResponseFriendsGet:(NSDictionary *) responseObject {
   
    self = [super init];
    if (self) {
      
        self.firstName = [responseObject objectForKey:@"first_name"];
        self.lastName = [responseObject objectForKey:@"last_name"];
        self.friendID = [responseObject objectForKey:@"user_id"];
        
        // avatar100
        NSString *stringAvatar100URL = [responseObject objectForKey:@"photo_100"];
        if (stringAvatar100URL) {
            self.avatar100URL = [NSURL URLWithString:stringAvatar100URL];
        }
        
        // birth date
        NSString *stringDateOfBirth = [responseObject objectForKey:@"bdate"];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        
        [dateFormat setDateFormat:@"dd.MM.yyyy"];
        if (stringDateOfBirth) {
        self.dateOfBirth = [dateFormat dateFromString:stringDateOfBirth];
        }
        
        // online?
        NSString *online = [responseObject objectForKey:@"online"];
        self.online = [online boolValue];
    }
    return self;
}

- (instancetype)initWithServerResponseUserGet:(NSDictionary *)responseObject {
    
    self = [super init];
    if (self) {
        
        self.firstName = [responseObject objectForKey:@"first_name"];
        self.lastName = [responseObject objectForKey:@"last_name"];
        
        // avatarMaxSize
        NSString *stringAvatarMaxSize = [responseObject objectForKey:@"photo_max"];
        if (stringAvatarMaxSize) {
            self.avatarMaxSize = [NSURL URLWithString:stringAvatarMaxSize];
        }
        
        // birth date
        NSString *stringDateOfBirth = [responseObject objectForKey:@"bdate"];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        
        [dateFormat setDateFormat:@"dd.MM.yyyy"];
        if (stringDateOfBirth) {
            self.dateOfBirth = [dateFormat dateFromString:stringDateOfBirth];
        }
        
        // city
        if ([responseObject objectForKey:@"city"]) {
            self.city = [responseObject objectForKey:@"city"];
        }
        
        // last seen
        NSDictionary *dict = [responseObject objectForKey:@"last_seen"];
        
        NSTimeInterval timeInterval = [[dict objectForKey:@"time"] doubleValue] ;
                
        if (timeInterval) {
            self.lastSeenDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
        }
        
        // online?
        NSString *online = [responseObject objectForKey:@"online"];
        self.online = [online boolValue];
        
        // followers count
        NSString *countFollowers = [responseObject objectForKey:@"followers_count"];
        
        self.followersCount = [countFollowers floatValue];
        
    }
    return self;
}

- (instancetype)initWithServerResponseFollowersGet:(NSDictionary *) responseObject {
    
    self = [super init];
    if (self) {
        
        self.firstName = [responseObject objectForKey:@"first_name"];
        self.lastName = [responseObject objectForKey:@"last_name"];
        self.friendID = [responseObject objectForKey:@"uid"];
                
        // avatar100
        NSString *stringAvatar100URL = [responseObject objectForKey:@"photo_100"];
        if (stringAvatar100URL) {
            self.avatar100URL = [NSURL URLWithString:stringAvatar100URL];
        }
        
        // skype
        if ([responseObject objectForKey:@"skype"]) {
            self.skype = [responseObject objectForKey:@"skype"];
        }
    }
    return self;
}











@end
