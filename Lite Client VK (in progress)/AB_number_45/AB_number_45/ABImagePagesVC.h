//
//  ABImagePagesVC.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 09.08.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABImagePagesVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;

@property (strong, nonatomic) NSString *imageStringURL;
@property (assign, nonatomic) NSUInteger pageIndex;

@end
