//
//  ViewController.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 06.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABUserFriendsTVC.h"
#import "ABServerManager.h"
#import "ABUser.h"
#import "UIImageView+AFNetworking.h"
#import "ABUserInfoTVC.h"
#import "ABFriendCell.h"

static NSInteger friendsInRequest = 20;
static NSString *showFriendIdentifier = @"showFriend";

@interface ABUserFriendsTVC ()

@property (strong, nonatomic) NSMutableArray *friendsArray;
@property (strong, nonatomic) NSString *friendID;
@property (strong, nonatomic) NSString *userID;

@property (nonatomic, assign) BOOL firstTimeAppear;

@end

@implementation ABUserFriendsTVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.friendsArray = [NSMutableArray array];
    
    self.firstTimeAppear = YES;
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if (self.firstTimeAppear) {
        
        self.firstTimeAppear = NO;
        
        [[ABServerManager sharedManager] authorizeUser:^(ABUser *user) {
            
            self.userID = user.userID;
                        
            if (self.userID) {
                [self getFriends];
            }
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - API

- (void) getFriends {
    
    [[ABServerManager sharedManager]
     getFriendsForUserWithID:self.userID
     withOffset:self.friendsArray.count
     count:friendsInRequest
     onSuccess:^(NSArray *friends) {
         
         [self.friendsArray addObjectsFromArray:friends];
         
         NSMutableArray *newPaths = [NSMutableArray array];
         
         for (int i = (int)self.friendsArray.count - (int)friends.count;
              i < self.friendsArray.count; i++) {
             
             [newPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
         }
         
         [self.tableView beginUpdates];
         
         [self.tableView insertRowsAtIndexPaths:newPaths
                               withRowAnimation:UITableViewRowAnimationMiddle];
         
         [self.tableView endUpdates];
         
     } onFailure:^(NSError *error, NSInteger statusCode) {
         NSLog(@"error = %@, status code = %li", [error localizedDescription], statusCode);
     }];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.friendsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *friendCellIdentifier = @"FriendCell";
    
    ABFriendCell *friendCell = [tableView dequeueReusableCellWithIdentifier:friendCellIdentifier];
    
    friendCell.detailTextLabel.text = @"";
    
    if (!friendCell) {
        friendCell = [[ABFriendCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                            reuseIdentifier:friendCellIdentifier];
    }
    
    if (indexPath.row == (self.friendsArray.count - 1)) {
        [self getFriends];
    }
    
    ABUser *friend = [self.friendsArray objectAtIndex:indexPath.row];

    // name
    friendCell.textLabel.text = [NSString stringWithFormat:@"%@ %@", friend.firstName, friend.lastName];
    
    // birth day
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    
    NSString *stringFromBirthDateUser = [dateFormat stringFromDate:friend.dateOfBirth];
    
    if (stringFromBirthDateUser) {
        friendCell.detailTextLabel.text = [NSString stringWithFormat:@"%@", stringFromBirthDateUser];
    }
    
    // avatar
    NSURLRequest *request = [NSURLRequest requestWithURL:friend.avatar100URL];
    
    __weak ABFriendCell *weakFriendCell = friendCell;
    
    friendCell.imageView.image = nil;
    
    [friendCell.imageView
     setImageWithURLRequest:request
     placeholderImage:nil
     success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
         
         [UIView animateWithDuration:0.5f
                               delay:0.f
                             options:UIViewAnimationOptionTransitionCrossDissolve
                          animations:^{
         
                              weakFriendCell.imageView.image = image;
                              [weakFriendCell layoutSubviews];
                              
                          }
                          completion:^(BOOL finished) {
                          }];
     }
     failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
         NSLog(@"error = %@", [error localizedDescription]);
     }];
    
    // online?
    
    if (friend.online) {
        friendCell.onlineLabel.textColor = [UIColor greenColor];
        friendCell.onlineLabel.text = @"ONline";
    } else {
        friendCell.onlineLabel.textColor = [UIColor redColor];
        friendCell.onlineLabel.text = @"OFFline";
    }
    
    return friendCell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ABUser *friend = [self.friendsArray objectAtIndex:indexPath.row];
    
    self.friendID = friend.friendID;

    [self performSegueWithIdentifier:showFriendIdentifier sender:nil];
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:showFriendIdentifier]){
        ABUserInfoTVC *infoTVC = (ABUserInfoTVC *)segue.destinationViewController;
        infoTVC.userID = self.friendID;
        [[UIDevice currentDevice] setValue: [NSNumber numberWithInteger: UIInterfaceOrientationPortrait] forKey:@"orientation"];
    }
}









@end
