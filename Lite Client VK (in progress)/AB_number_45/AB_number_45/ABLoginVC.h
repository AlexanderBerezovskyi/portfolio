//
//  ABLoginVC.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 24.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABAccessToken;

typedef void (^ABLoginCompletionBlock)(ABAccessToken *token);

@interface ABLoginVC : UIViewController

- (id) initWithCompletionBlock:(ABLoginCompletionBlock) completionBlock;

@end
