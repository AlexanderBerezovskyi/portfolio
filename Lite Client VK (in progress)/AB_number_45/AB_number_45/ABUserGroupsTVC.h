//
//  ABUserGroupsTVC.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 13.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABUserGroupsTVC : UITableViewController

@property (strong, nonatomic) NSString *userID;

@end
