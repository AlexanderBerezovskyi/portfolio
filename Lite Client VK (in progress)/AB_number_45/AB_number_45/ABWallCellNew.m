//
//  ABWallCellNew.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 28.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABWallCellNew.h"
#import "ABUtilities.h"
#import "UIImageView+AFNetworking.h"

static NSString *collectionViewCellIdentifier = @"collectionViewCellIdentifier";

@interface ABWallCellNew () <UICollectionViewDataSource, UICollectionViewDelegate>

@end



@implementation ABWallCellNew

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.imagesCollectionView reloadData];
    
    [self.imagesCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:collectionViewCellIdentifier];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
    
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.imagesCollectionViewArray.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [self.imagesCollectionView
     dequeueReusableCellWithReuseIdentifier:collectionViewCellIdentifier forIndexPath:indexPath];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.contentView.frame];
    imageView.contentMode = UIViewContentModeScaleToFill;
    
    [cell.contentView addSubview:imageView];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[self.imagesCollectionViewArray objectAtIndex:indexPath.row]]];
    
    __weak UIImageView *weakImageView = imageView;
        
    [imageView
     setImageWithURLRequest:request
     placeholderImage:nil
     success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                              
          weakImageView.image = image;
          [cell layoutSubviews];

     }
     failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
         NSLog(@"error = %@", [error localizedDescription]);
     }];
    
    return cell;
}


#pragma mark - Reduce Methods

- (CGFloat) getHeightCollectionViewWithItemsArray:(NSArray *)itemsArray andContentWidth:(NSInteger)contentWidth {
    
    if (itemsArray.count > 1) {
        NSInteger itemsCount = itemsArray.count;
        NSInteger maxCountInLine = (contentWidth - 20) / 100;
        
        CGFloat lines = ceilf ((CGFloat) itemsCount / maxCountInLine);
        
        return (CGFloat) (100 * lines + 10 * (lines - 1));
        
    } else {
        return 0.f;
    }
}


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    self.itemBlock(indexPath, self.imagesCollectionViewArray);
}


#pragma mark - Actions

- (IBAction) likeMakeAction:(UIButton *)sender {
    
    if ([self.likeButton.currentImage isEqual:[UIImage imageNamed:@"heart_3.png"]]) {
        
        [self.likeButton setImage:[UIImage imageNamed:@"heart_1.ico"] forState:UIControlStateNormal];
        
    } else {
        [self.likeButton setImage:[UIImage imageNamed:@"heart_3.png"] forState:UIControlStateNormal];
    }
    
}















@end
