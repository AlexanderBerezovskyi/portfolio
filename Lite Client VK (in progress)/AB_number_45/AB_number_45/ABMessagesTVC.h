//
//  ABMessagesTVC.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 06.09.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABMessagesTVC : UITableViewController

@property (strong, nonatomic) NSString *userID;

@end
