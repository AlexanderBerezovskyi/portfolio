//
//  ABServerManager.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 07.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ABUser;



@interface ABServerManager : NSObject

+ (ABServerManager *) sharedManager;


#pragma mark - Get Token

- (void) authorizeUser: (void (^)(ABUser *user)) completion;

- (void) getUser:(NSString *) userID
       onSuccess:(void(^) (ABUser *user)) success
       onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure;


#pragma mark - Methods without Token

- (void) getFriendsForUserWithID:(NSString *) userID
                      withOffset:(NSInteger) offset
                           count:(NSInteger) count
                       onSuccess:(void(^) (NSArray *friends)) success
                       onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure;

- (void) getUserInfoWithID:(NSString *) userID
                 onSuccess:(void(^) (ABUser *user)) success
                 onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure;

- (void) getCityNameByID:(NSString *) cityID
               onSuccess:(void(^) (NSString *cityName)) success
               onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure;

- (void) getUserSubscriptionsCountWithID:(NSString *)userID
                               onSuccess:(void (^)(NSInteger subscriptionsCount))success
                               onFailure:(void (^)(NSError *error, NSInteger statusCode))failure;

- (void) getUserFollowersWithID:(NSString *) userID
                     withOffset:(NSInteger) offset
                          count:(NSInteger) count
                      onSuccess:(void(^) (NSArray *followers)) success
                      onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure;

- (void) getUserSubscriptionsGroupsByUserID:(NSString *) userID
                                 withOffset:(NSInteger) offset
                                      count:(NSInteger) count
                                  onSuccess:(void(^) (NSArray *subscriptionsGroups)) success
                                  onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure;

- (void) getWallPostsOfUserID:(NSString *) userID
                   withOffset:(NSInteger) offset
                        count:(NSInteger) count
                    onSuccess:(void(^) (NSArray *posts)) success
                    onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure;

- (void) addLikeOnWallPostWithPostID:(NSString *)postID
                          andOwnerID:(NSString *)ownerID
                           onSuccess:(void (^)(NSUInteger likesCount))success
                           onFailure:(void (^)(NSError *error, NSInteger statusCode))failure;

- (void) deleteLikeOnWallPostWithPostID:(NSString *)postID
                             andOwnerID:(NSString *)ownerID
                              onSuccess:(void (^)(NSUInteger likesCount))success
                              onFailure:(void (^)(NSError *error, NSInteger statusCode))failure;

- (void) addPostOnWallUserWithID:(NSString *)ownerID
                        withText:(NSString *)currentText
                       onSuccess:(void (^)(NSString *postID))success
                       onFailure:(void (^)(NSError *error, NSInteger statusCode))failure;

- (void) getMessagesToUserID:(NSString *) userID
                  withOffset:(NSInteger) offset
                       count:(NSInteger) count
                   onSuccess:(void(^) (NSArray *messages)) success
                   onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure;




@end
