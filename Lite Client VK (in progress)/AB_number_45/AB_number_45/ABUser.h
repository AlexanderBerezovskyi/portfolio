//
//  ABUser.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 07.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABUser : NSObject

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSDate *dateOfBirth;
@property (strong, nonatomic) NSURL *avatar100URL;
@property (strong, nonatomic) NSString *friendID;
@property (assign, nonatomic) Boolean online;

@property (strong, nonatomic) NSURL *avatarMaxSize;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSDate *lastSeenDate;
@property (assign, nonatomic) NSInteger followersCount;

@property (strong, nonatomic) NSString *skype;

@property (strong, nonatomic) NSString *userID;


- (instancetype)initWithServerResponseUserGetAuthorize:(NSDictionary *)responseObject;
- (instancetype)initWithServerResponseFriendsGet:(NSDictionary *) responseObject;
- (instancetype)initWithServerResponseUserGet:(NSDictionary *) responseObject;
- (instancetype)initWithServerResponseFollowersGet:(NSDictionary *) responseObject;

@end
