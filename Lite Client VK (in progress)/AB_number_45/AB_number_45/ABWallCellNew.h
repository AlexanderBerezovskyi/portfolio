//
//  ABWallCellNew.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 28.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABWallCellNew : UITableViewCell

typedef void (^ItemBlock)(NSIndexPath*, NSArray*);

@property (copy, nonatomic) ItemBlock itemBlock;

@property (strong, nonatomic) NSArray *imagesCollectionViewArray;
@property (assign, nonatomic) NSInteger contentWidth;

@property (weak, nonatomic) IBOutlet UIImageView *postAutorImageView;
@property (weak, nonatomic) IBOutlet UILabel *postMessageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *postImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameAuthorPostLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *imagesCollectionView;

@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *postDateLabel;



// constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;

- (CGFloat) getHeightCollectionViewWithItemsArray:(NSArray *)itemsArray andContentWidth:(NSInteger)contentWidth;

@end
