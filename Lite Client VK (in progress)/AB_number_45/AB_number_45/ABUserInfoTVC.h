//
//  ABUserInfoTVC.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 11.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABUserInfoTVC : UITableViewController

@property (strong, nonatomic) NSString *userID;

@end
