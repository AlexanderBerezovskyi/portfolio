//
//  ABMessage.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 06.09.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABMessage : NSObject

@property (strong, nonatomic) NSString *bodyText;
@property (strong, nonatomic) NSString *messageFromID;
@property (strong, nonatomic) NSDate *messageSendDate;

- (instancetype)initWithServerResponseMessageGet:(NSDictionary *) responseObject;

@end
