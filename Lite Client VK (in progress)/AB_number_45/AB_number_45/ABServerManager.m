//
//  ABServerManager.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 07.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABServerManager.h"
#import "AFNetworking.h"
#import "ABUser.h"
#import "ABGroup.h"
#import "ABLoginVC.h"
#import "ABAccessToken.h"
#import "ABWall.h"
#import "ABMessage.h"

@interface ABServerManager ()

@property (strong, nonatomic) AFHTTPSessionManager *requestOperationManager;
@property (nonatomic, strong) ABAccessToken *accessToken;

@end

@implementation ABServerManager


#pragma mark - Get Token Methods

// ID приложения:	6121706

- (void) authorizeUser: (void (^)(ABUser *user)) completion {
    
    ABLoginVC *vc = [[ABLoginVC alloc] initWithCompletionBlock:^(ABAccessToken *token) {
        self.accessToken = token;
        
        if (token) {
            
            [self getUser:self.accessToken.userID
                onSuccess:^(ABUser *user) {
                    
                    if (completion) {
                        completion (user);
                    }
                }
                onFailure:^(NSError *error, NSInteger statusCode) {
                    
                    if (completion) {
                        completion (nil);
                    }
                }];
            
        } else if (completion) {
            
            completion (nil);
        }
        
    }];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    
    UIViewController *mainVC = [[[[UIApplication sharedApplication] windows] firstObject] rootViewController];
    
    [mainVC presentViewController:nav
                         animated:YES
                       completion:nil];
}

- (void) getUser:(NSString *) userID
       onSuccess:(void(^) (ABUser *user)) success
       onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            userID,         @"user_ids",
                            @"nom",         @"name_case", nil];    
    
    [self.requestOperationManager
     GET:@"users.get"
     parameters:params
     progress:nil
     
     success:^(NSURLSessionTask *task, id responseObject) {
         
          NSArray *dictsArray = [responseObject objectForKey:@"response"];
          
          if ([dictsArray count] > 0) {
              ABUser *user = [[ABUser alloc] initWithServerResponseUserGetAuthorize:[dictsArray firstObject]];
              
              if (success) {
                  success (user);
              } else {
                  
                  if (failure) {
                      
                      NSHTTPURLResponse* httpResponse =
                      (NSHTTPURLResponse *)task;
                      
                      if (failure) {
                          failure(nil, httpResponse.statusCode);
                      }
                  }
              }
          }
     }
     
     failure:^(NSURLSessionTask *operation, NSError *error) {
                                  
          NSLog(@"Error: %@", error);
          
          NSHTTPURLResponse* httpResponse =
          (NSHTTPURLResponse *)operation;
          
          if (failure) {
              failure(error, httpResponse.statusCode);
          }
         
     }];
}


#pragma mark - API methods

+ (ABServerManager *) sharedManager {
    
    static ABServerManager *manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ABServerManager alloc] init];
    });
    return manager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        NSURL *baseURLmethodsVK = [NSURL URLWithString:@"https://api.vk.com/method/"];
        
        self.requestOperationManager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURLmethodsVK];
    }
    
    return self;
}

- (void) getFriendsForUserWithID:(NSString *) userID
                      withOffset:(NSInteger) offset
                           count:(NSInteger) count
                    onSuccess:(void(^) (NSArray *friends)) success
                    onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *friendsParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     userID,                      @"user_id",
     @"name",                     @"order",
     @(count),                    @"count",
     @(offset),                   @"offset",
     @"nom",                      @"name_case",
     @"bdate, photo_100, online", @"fields", nil];
    
    [self.requestOperationManager
     GET:@"friends.get"
     parameters:friendsParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         NSMutableArray *friendsArray = [NSMutableArray array];
         
         NSArray *responseArray = [responseObject objectForKey:@"response"];

         for (NSDictionary *dict in responseArray) {
             ABUser *user = [[ABUser alloc] initWithServerResponseFriendsGet:dict];
             [friendsArray addObject:user];
         }
         
         if (success) {
             success (friendsArray);
         }
    }
     
     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         
         if (failure) {
             failure(error, task.error.code);
         }
         NSLog(@"error = %@", [error localizedDescription]);
    }];
}

- (void) getUserInfoWithID:(NSString *) userID
                 onSuccess:(void(^) (ABUser *user)) success
                 onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *userParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     userID,                                                        @"user_ids",
     @"nom",                                                        @"name_case",
     @"bdate, city, photo_max, last_seen, online, followers_count", @"fields", nil];
    
    [self.requestOperationManager
     GET:@"users.get"
     parameters:userParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                  
         NSArray *responseArray = [responseObject objectForKey:@"response"];
         NSDictionary *responseDict = [responseArray objectAtIndex:0];
         
         ABUser *user = [[ABUser alloc] initWithServerResponseUserGet:responseDict];
         
         if (success) {
             success (user);
         }
     }
     
     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"error = %@", [error localizedDescription]);
         
         if (failure) {
             failure(error, task.error.code);
         }
     }];
}

- (void) getCityNameByID:(NSString *) cityID
               onSuccess:(void(^) (NSString *cityName)) success
               onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *cityParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     cityID, @"city_ids", nil];

    [self.requestOperationManager
     GET:@"database.getCitiesById"
     parameters:cityParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         NSArray *responseArray = [responseObject objectForKey:@"response"];
         
         if (responseArray.count > 0) {
             NSDictionary *responseDict = [responseArray objectAtIndex:0];
             
             NSString *cityName = [responseDict objectForKey:@"name"];
             
             if (success) {
                 success (cityName);
             }
         }
     }
     
     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"error = %@", [error localizedDescription]);
         
         if (failure) {
             failure(error, task.error.code);
         }
     }];
}

- (void) getUserSubscriptionsCountWithID:(NSString *)userID
                               onSuccess:(void (^)(NSInteger subscriptionsCount))success
                               onFailure:(void (^)(NSError *error, NSInteger statusCode))failure {
    
    NSDictionary *userParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     userID,                    @"user_id",
     @1,                        @"extended",
     self.accessToken.token,    @"access_token", nil];
    
    [self.requestOperationManager
     GET:@"groups.get"
     parameters:userParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

         NSArray *groupsArray = [[NSArray alloc] initWithArray:[responseObject objectForKey:@"response"]];
     
         if (groupsArray.count > 0) {
         
         NSString *stringSubscriptionsCount = [groupsArray firstObject];
         NSInteger subscriptionsCount = [stringSubscriptionsCount integerValue];
             
             if (success) {
                 success (subscriptionsCount);
             }
         }
     }
     
     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"error = %@", [error localizedDescription]);
         
         if (failure) {
             failure(error, task.error.code);
         }
     }];
}

- (void) getUserFollowersWithID:(NSString *) userID
                     withOffset:(NSInteger) offset
                          count:(NSInteger) count
                      onSuccess:(void(^) (NSArray *followers)) success
                      onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *followersParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     userID,                         @"user_id",
     @(count),                       @"count",
     @(offset),                      @"offset",
     @"nom",                         @"name_case",
     @"photo_100, connections",      @"fields", nil];
    
    [self.requestOperationManager
     GET:@"users.getFollowers"
     parameters:followersParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         NSMutableArray *followersArray = [NSMutableArray array];

         NSDictionary *responseDict = [responseObject objectForKey:@"response"];
         NSArray *itemsArray = [responseDict objectForKey:@"items"];
         
         for (NSDictionary *dict in itemsArray) {
                          
             ABUser *user = [[ABUser alloc] initWithServerResponseFollowersGet:dict];
             [followersArray addObject:user];
         }

         if (success) {
             success (followersArray);
         }
     }
     
     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"error = %@", [error localizedDescription]);
         
         if (failure) {
             failure(error, task.error.code);
         }
     }];
}

- (void) getUserSubscriptionsGroupsByUserID:(NSString *) userID
                                 withOffset:(NSInteger) offset
                                      count:(NSInteger) count
                                  onSuccess:(void(^) (NSArray *subscriptionsGroups)) success
                                  onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *groupParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     userID,                         @"user_id",
     @1,                             @"extended",
     @(count),                       @"count",
     @(offset),                      @"offset",
     @"status",                      @"fields",
     self.accessToken.token,         @"access_token", nil];
    
    [self.requestOperationManager
     GET:@"groups.get"
     parameters:groupParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         NSMutableArray *groupsArray = [NSMutableArray new];
         
         NSMutableArray *responseArray = [NSMutableArray arrayWithArray: [responseObject objectForKey:@"response"]];
         
         if (responseArray.count > 0) {
             [responseArray removeObjectAtIndex:0];
         }
         
         for (NSDictionary *dict in responseArray) {
             
             ABGroup *group = [[ABGroup alloc] initWithServerResponseGroupGet:dict];
            
             [groupsArray addObject:group];
         }
         
         if (success) {
             success (groupsArray);
         }
     }
     
     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"error = %@", [error localizedDescription]);
         
         if (failure) {
             failure(error, task.error.code);
         }
     }];
}

- (void) getWallPostsOfUserID:(NSString *) userID
                   withOffset:(NSInteger) offset
                        count:(NSInteger) count
                    onSuccess:(void(^) (NSArray *posts)) success
                    onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *wallParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     userID,                      @"owner_id",
     @(count),                    @"count",
     @(offset),                   @"offset",
     @(1),                        @"extended",
     self.accessToken.token,      @"access_token",nil];
        
    [self.requestOperationManager
     GET:@"wall.get"
     parameters:wallParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                           
         NSDictionary *responseDict = [responseObject objectForKey:@"response"];

//       if need groups - uncomment
         NSDictionary *groupsDictionary = [responseDict objectForKey:@"groups"];
         
         NSDictionary *usersDictionary = [responseDict objectForKey:@"profiles"];
         
         NSMutableArray *wallPostsArray = [NSMutableArray array];
         
         for (id object in [responseDict objectForKey:@"wall"]) {
             
             if ([object isKindOfClass:[NSDictionary class]]) {
                 [wallPostsArray addObject:object];
             }
         }
         
         NSMutableArray *postsArray = [NSMutableArray array];
         
         for (NSDictionary *dictPost in wallPostsArray) {
             
             NSString *postFromID = [NSString stringWithFormat:@"%@", [dictPost objectForKey:@"from_id"]];
             
             if ([postFromID hasPrefix:@"-"]) {
                 
                 NSString *groupPostID = [postFromID substringFromIndex:1];
                
                 for (NSDictionary *dictGroup in groupsDictionary) {
                     
                     NSString *groupID = [NSString stringWithFormat:@"%@", [dictGroup objectForKey:@"gid"]];
                     
                     if ([groupID isEqualToString:groupPostID]) {
                         
                         ABWall *wall = [[ABWall alloc]
                                         initWithServerResponseWallPostsGet:dictPost
                                         userWhoPosted:nil
                                         groupWhoPosted:dictGroup];
                         
                         [postsArray addObject:wall];                        
                     }
                 }
                 
             } else {
             
                 for (NSDictionary *dictUser in usersDictionary) {
                     
                     NSString *userID = [NSString stringWithFormat:@"%@", [dictUser objectForKey:@"uid"]];
                     
                     if ([userID isEqual:postFromID]) {
                         
                         ABWall *wall = [[ABWall alloc]
                                         initWithServerResponseWallPostsGet:dictPost
                                         userWhoPosted:dictUser
                                         groupWhoPosted:nil];
                         
                         [postsArray addObject:wall];
                     }
                 }
             }
         }
         
         if (success) {
             success (postsArray);
         }
     }
     
     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         
         if (failure) {
             failure(error, task.error.code);
         }
         NSLog(@"error = %@", [error localizedDescription]);
     }];
}

- (void) addLikeOnWallPostWithPostID:(NSString *)postID
                          andOwnerID:(NSString *)ownerID
                           onSuccess:(void (^)(NSUInteger likesCount))success
                           onFailure:(void (^)(NSError *error, NSInteger statusCode))failure {
    
    NSDictionary *userParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     @"post",                       @"type",
     ownerID,                       @"owner_id",
     postID,                        @"item_id",
     self.accessToken.token,        @"access_token", nil];
    
    [self.requestOperationManager
     GET:@"likes.add"
     parameters:userParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         NSDictionary *responseDict = [responseObject objectForKey:@"response"];
         
         NSUInteger countLikes = [[responseDict objectForKey:@"likes"] unsignedIntegerValue];
         
         if (success) {
             success (countLikes);
         }
     }
     
     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"error = %@", [error localizedDescription]);
         
         if (failure) {
             failure(error, task.error.code);
         }
     }];
}

- (void) deleteLikeOnWallPostWithPostID:(NSString *)postID
                             andOwnerID:(NSString *)ownerID
                              onSuccess:(void (^)(NSUInteger likesCount))success
                              onFailure:(void (^)(NSError *error, NSInteger statusCode))failure {
    
    NSDictionary *userParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     @"post",                       @"type",
     ownerID,                       @"owner_id",
     postID,                        @"item_id",
     self.accessToken.token,        @"access_token", nil];
    
    [self.requestOperationManager
     GET:@"likes.delete"
     parameters:userParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         NSDictionary *responseDict = [responseObject objectForKey:@"response"];
         
         NSUInteger countLikes = [[responseDict objectForKey:@"likes"] unsignedIntegerValue];
         
         if (success) {
             success (countLikes);
         }
     }
     
     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"error = %@", [error localizedDescription]);
         
         if (failure) {
             failure(error, task.error.code);
         }
     }];
}

- (void) addPostOnWallUserWithID:(NSString *)ownerID
                        withText:(NSString *)currentText
                       onSuccess:(void (^)(NSString *postID))success
                       onFailure:(void (^)(NSError *error, NSInteger statusCode))failure {
    
    NSDictionary *postParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     ownerID,                       @"owner_id",
     currentText,                   @"message",
     self.accessToken.token,        @"access_token", nil];
    
    [self.requestOperationManager
     POST:@"wall.post"
     parameters:postParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         NSDictionary *dictPost = [responseObject objectForKey:@"response"];
         NSString *currentPostID = [dictPost objectForKey:@"post_id"];
         
         if (success) {
             success (currentPostID);
         }
         
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         
         NSLog(@"error = %@", [error localizedDescription]);
         
         if (failure) {
             failure(error, task.error.code);
         }
     }];
}


- (void) getMessagesToUserID:(NSString *) userID
                  withOffset:(NSInteger) offset
                       count:(NSInteger) count
                   onSuccess:(void(^) (NSArray *messages)) success
                   onFailure:(void(^) (NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *messagesParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     userID,                         @"user_id",
     @(count),                       @"count",
     @(offset),                      @"offset",
     self.accessToken.token,         @"access_token", nil];
    
    [self.requestOperationManager
     GET:@"messages.getHistory"
     parameters:messagesParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                  
         NSMutableArray *messagesArray = [NSMutableArray array];

         NSMutableArray *responseDictArray = [NSMutableArray arrayWithArray: [responseObject objectForKey:@"response"]];
         [responseDictArray removeObjectAtIndex:0];
         
         NSLog(@"responseMessagesDict = %@", responseDictArray);
         
         for (NSDictionary *dict in responseDictArray) {

             ABMessage *message = [[ABMessage alloc] initWithServerResponseMessageGet:dict];
             [messagesArray addObject:message];
         }

         if (success) {
             success (messagesArray);
         }
     }
     
     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"error = %@", [error localizedDescription]);
         
         if (failure) {
             failure(error, task.error.code);
         }
     }];
}











@end
