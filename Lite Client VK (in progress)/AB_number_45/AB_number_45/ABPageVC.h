//
//  ABPageVC.h
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 09.08.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABPageVC : UIPageViewController

@property (strong, nonatomic) NSArray *itemsArray;
@property (strong, nonatomic) NSIndexPath *itemIndexPath;

@end
