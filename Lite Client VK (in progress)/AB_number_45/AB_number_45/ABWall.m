//
//  ABWall.m
//  AB_number_45
//
//  Created by Alexander Berezovskyy on 25.07.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABWall.h"

@implementation ABWall

- (instancetype)initWithServerResponseWallPostsGet:(NSDictionary *) responseObject
                                     userWhoPosted:(NSDictionary *) userPosted
                                    groupWhoPosted:(NSDictionary *) groupPosted
{
    self = [super init];
    
    if (self) {
        
        // post text
        self.postText = [responseObject objectForKey:@"text"];
        
        // name, photo (ava)
        if (userPosted) {
            
            self.nameAuthorPost = [NSString stringWithFormat:@"%@ %@",
                                   [userPosted objectForKey:@"first_name"],
                                   [userPosted objectForKey:@"last_name"]];
            
            NSString *stringUserPhoto = [userPosted objectForKey:@"photo_medium_rec"];
            if (stringUserPhoto) {
                self.photoUser = [NSURL URLWithString:stringUserPhoto];
            }
        
        } else {
            self.nameAuthorPost = [NSString stringWithFormat:@"%@",
                                   [groupPosted objectForKey:@"name"]];
            
            NSString *stringGroupPhoto = [groupPosted objectForKey:@"photo_medium"];
            if (stringGroupPhoto) {
                self.photoUser = [NSURL URLWithString:stringGroupPhoto];
            }
        }
    
        // photos
        self.postImagesArray = [NSMutableArray array];
        
        if ([responseObject objectForKey:@"attachments"]) {
            
            for (NSDictionary *dictAttachment in [responseObject objectForKey:@"attachments"]) {
                
                if ([[dictAttachment objectForKey:@"type"] isEqualToString:@"photo"]) {
                    
                    NSDictionary *photoDict = [dictAttachment objectForKey:@"photo"];
                                        
                    [self.postImagesArray addObject:[photoDict objectForKey:@"src_big"]];
                    
                    if (!self.postPhotoBig) {
                        
                        NSString *stringBigPhoto = [photoDict objectForKey:@"src_big"];
                        self.postPhotoBig = [NSURL URLWithString:stringBigPhoto];
                    }
                }
            }
        }
        
        // ID of post
        self.wallPostID = [responseObject objectForKey:@"id"];
        
        // likes
        NSDictionary *likesInfoDict = [responseObject objectForKey:@"likes"];
        
        self.likesCount = [[likesInfoDict objectForKey:@"count"] unsignedIntegerValue];
        self.canMakeLike = [[likesInfoDict objectForKey:@"can_like"] boolValue];
        
        // post created date
        NSInteger globalCreatedSeconds = [[responseObject objectForKey:@"date"] integerValue];
        NSDate *globalCreatedDate = [NSDate dateWithTimeIntervalSince1970:globalCreatedSeconds];
        
        self.postCreatedDate = globalCreatedDate;
        
        // owner ID
        self.ownerID = [responseObject objectForKey:@"to_id"];
        
        // can post on wall
        NSDictionary *commentsDict = [responseObject objectForKey:@"comments"];
        self.canPost = [[commentsDict objectForKey:@"can_post"] boolValue];
        NSLog(@"self.canPost = %d", self.canPost);
    }

    return self;
}

@end
