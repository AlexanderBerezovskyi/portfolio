//
//  ViewController.m
//  AB_number_25
//
//  Created by Alexander Berezovskyy on 20.03.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ViewController.h"
#import <AudioToolbox/AudioToolbox.h>

@interface ViewController ()

typedef enum {
    
    ABFirstValueEdit,
    ABSecondValueEdit
    
} ABValueEdit;

typedef enum {
    
    ABBeginClearSecondValue,
    ABFinishClearSecondValue
    
} ABClearSecondValue;

@property (strong, nonatomic) NSString *currentNumber;

@property (assign, nonatomic) CGFloat firstValue;
@property (assign, nonatomic) CGFloat secondValue;

@property (assign, nonatomic) NSInteger currentValue;                   // flag
@property (assign, nonatomic) NSInteger stateOfClearSecondValue;        // flag

@property (assign, nonatomic) NSInteger currentTag;

@property (weak, nonatomic) IBOutlet UILabel *outsideBorderLabel;       // border out
@property (weak, nonatomic) IBOutlet UILabel *totalValueLabel;          // border in

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *numbersButtonsCollection;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *operationsButtonsCollection;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *otherSignsButtonsCollection;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *allButtonsCollection;


- (IBAction)showNumberAction:(UIButton *)sender;
- (IBAction)operationAction:(UIButton *)sender;
- (IBAction)equalAction:(UIButton *)sender;
- (IBAction)cancelAction:(UIButton *)sender;
- (IBAction)signMinusPlusAction:(UIButton *)sender;
- (IBAction)percentAction:(UIButton *)sender;
- (IBAction)dotAction:(UIButton *)sender;

- (NSString *) calculate:(NSInteger)senderTag;
- (NSString *) setDecimalNumberStyleWithMaxIntDigits:(NSUInteger)maxIntDigits andMaxFractionDigits:(NSUInteger)maxFractionDigits forValue:(CGFloat)value;

- (CGFloat) numberFromString:(NSString *)converterString;
- (CGFloat) createValueFromNumbers:(NSInteger)number;

- (void) playSoundWithName:(NSString *)soundName andExtension:(NSString *)fileExtension;
- (void) object:(UIView *)currentObject withBorderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth cornerRadius:(CGFloat)cornerRadius shadowOpacity:(CGFloat)shadowOpacity shadowColor:(UIColor *)shadowColor;
- (void) nextOperation;

@end


@implementation ViewController

#pragma mark - Initialization;

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.firstValue = 0.f;
        self.secondValue = 0.f;
        self.totalValueLabel.text = @"0";
        self.currentValue = ABFirstValueEdit;
        self.stateOfClearSecondValue = ABBeginClearSecondValue;
        self.currentTag = 0;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

#pragma mark - Borders & Font
    
    // Set text size
    
    CGFloat textSize = self.outsideBorderLabel.bounds.size.height / 7;

    // Set text style & font
    
    [self.totalValueLabel setFont:[UIFont fontWithName:@"American Typewriter"
                                                  size:textSize * 1.5f]];
    
    for (UIButton *currentButton in self.allButtonsCollection) {
        [currentButton.titleLabel setFont:[UIFont fontWithName:@"Noteworthy-Bold"
                                                          size:textSize]];
    }
    
    // Set borders, radius & shadows
    
    [self object:self.outsideBorderLabel
 withBorderColor:[UIColor whiteColor]
     borderWidth:4.f
    cornerRadius:self.outsideBorderLabel.bounds.size.height / 7
   shadowOpacity:0.1f
     shadowColor:[UIColor whiteColor]];
    
    for (UIButton *numberButton in self.numbersButtonsCollection) {
        
        [self object:numberButton
     withBorderColor:[UIColor whiteColor]
         borderWidth:4.f
        cornerRadius:numberButton.bounds.size.height / 7
       shadowOpacity:0.2f
         shadowColor:[UIColor whiteColor]];
    }
    
    for (UIButton *operationButton in self.operationsButtonsCollection) {
        
        [self object:operationButton
     withBorderColor:[UIColor greenColor]
         borderWidth:2.f
        cornerRadius:operationButton.bounds.size.height / 7
       shadowOpacity:0.05f
         shadowColor:[UIColor whiteColor]];
    }
    
    for (UIButton *signButton in self.otherSignsButtonsCollection) {
        
        [self object:signButton
     withBorderColor:[UIColor yellowColor]
         borderWidth:2.f
        cornerRadius:signButton.bounds.size.height / 7
       shadowOpacity:0.05f
         shadowColor:[UIColor whiteColor]];
    }
}


#pragma mark - Action buttons

- (IBAction)showNumberAction:(UIButton *)sender {

    [self playSoundWithName:@"button_sound_2" andExtension:@"mp3"];
 
    UIColor *currentColor = sender.backgroundColor;
    
    [UIView animateWithDuration:0.05f
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         sender.backgroundColor = [UIColor lightGrayColor];
                     }
                     completion:^(BOOL finished) {
                         sender.backgroundColor = currentColor;
                     }];
    
    // start
    
    if ([self.totalValueLabel.text isEqualToString:@"0"] || [self.totalValueLabel.text isEqualToString:@"-0"]) {
        self.totalValueLabel.text = @"";
    }
    
    // first value
    
    if (self.currentValue == ABFirstValueEdit) {
        
         self.firstValue = [self createValueFromNumbers:sender.tag];
    }
    
    // second value
    
    if (self.currentValue == ABSecondValueEdit) {
        
        if (self.stateOfClearSecondValue == ABBeginClearSecondValue) {
            self.totalValueLabel.text = @"";
            self.stateOfClearSecondValue = ABFinishClearSecondValue;
        }
        
        self.secondValue = [self createValueFromNumbers:sender.tag];
    }
}


- (IBAction)operationAction:(UIButton *)sender {
    
    [self playSoundWithName:@"other_buttons_2" andExtension:@"wav"];
    
    self.currentValue = ABSecondValueEdit;
    
    if (self.secondValue != 0) {
        
        [self nextOperation];
        
    }
    
    self.currentTag = sender.tag;
}


- (IBAction)equalAction:(UIButton *)sender {
    
    [self playSoundWithName:@"equal_button_sound" andExtension:@"wav"];
    
    if (self.secondValue != 0) {
        
        [self nextOperation];
        
        self.currentValue = ABFirstValueEdit;
    }
}

- (IBAction)cancelAction:(UIButton *)sender {
    
    [self playSoundWithName:@"cancel_3" andExtension:@"wav"];
    
    self.totalValueLabel.text = @"0";
    self.firstValue = 0;
    self.secondValue = 0;
    self.currentValue = ABFirstValueEdit;
    self.stateOfClearSecondValue = ABBeginClearSecondValue;
    self.currentTag = 0;
    
}
- (IBAction)signMinusPlusAction:(UIButton *)sender {
    
    [self playSoundWithName:@"other_buttons_2" andExtension:@"wav"];
    
    CGFloat tmpTotalValue = [self numberFromString:self.totalValueLabel.text];
    
    self.totalValueLabel.text = [NSString stringWithFormat:@"%@",
    [self setDecimalNumberStyleWithMaxIntDigits:9
                           andMaxFractionDigits:4
                                       forValue:-tmpTotalValue]];
    
    if (self.currentValue == ABFirstValueEdit) {
        
        self.firstValue = [self numberFromString:self.totalValueLabel.text];
        
    } else {
        
        self.secondValue = [self numberFromString:self.totalValueLabel.text];
    }
}

- (IBAction)percentAction:(UIButton *)sender {
    
    [self playSoundWithName:@"other_buttons_2" andExtension:@"wav"];
    
    if (self.currentValue == ABFirstValueEdit) {
        
        self.totalValueLabel.text = [NSString stringWithFormat:@"%@",
                                     [self setDecimalNumberStyleWithMaxIntDigits:9
                                                            andMaxFractionDigits:4
                                                                        forValue:self.firstValue/100]];
        
        self.firstValue = [self numberFromString:self.totalValueLabel.text];
        
    } else {
        
        self.secondValue = self.firstValue * self.secondValue/100;
        self.totalValueLabel.text = [NSString stringWithFormat:@"%@",
                                     [self setDecimalNumberStyleWithMaxIntDigits:9
                                                            andMaxFractionDigits:4
                                                                        forValue:self.secondValue]];
    }
}

- (IBAction)dotAction:(UIButton *)sender {
    
    [self playSoundWithName:@"other_buttons_2" andExtension:@"wav"];

    if (![self.totalValueLabel.text containsString:@"."]) {
        self.totalValueLabel.text = [self.totalValueLabel.text stringByAppendingString:@"."];
    }
}


#pragma mark - Calculate Method

- (NSString *) calculate:(NSInteger)senderTag {
    
    CGFloat result;
    
    switch (senderTag) {
            
        case 10:
            result = self.firstValue + self.secondValue;
            break;
        case 11:
            result = self.firstValue - self.secondValue;
            break;
        case 12:
            result = self.firstValue * self.secondValue;
            break;
        case 13:
            result = self.firstValue / self.secondValue;
            break;

        default:
            NSLog(@"Error");
            result = 0.123456f;
            break;
    }

    return [self setDecimalNumberStyleWithMaxIntDigits:9
                                  andMaxFractionDigits:4
                                              forValue:result];
}


#pragma mark - NSNumberFormatter

- (NSString *) setDecimalNumberStyleWithMaxIntDigits:(NSUInteger)maxIntDigits andMaxFractionDigits:(NSUInteger)maxFractionDigits forValue:(CGFloat)value {
    
    NSNumberFormatter *currentNumberFormat = [[NSNumberFormatter alloc] init];
    
    [currentNumberFormat setNumberStyle:NSNumberFormatterDecimalStyle];
    [currentNumberFormat setGroupingSeparator:@" "];
    [currentNumberFormat setGroupingSize:3];
    [currentNumberFormat setMaximumIntegerDigits:maxIntDigits];
    [currentNumberFormat setMaximumFractionDigits:maxFractionDigits];
    
    return [currentNumberFormat stringFromNumber:[NSNumber numberWithDouble:value]];
}

- (CGFloat) numberFromString:(NSString *)converterString {
    
    CGFloat tmpValue = [[converterString stringByReplacingOccurrencesOfString:@" "
                                                                   withString:@""] doubleValue];
    return tmpValue;
}


#pragma mark - Sounds

- (void) playSoundWithName:(NSString *)soundName andExtension:(NSString *)fileExtension {
    
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:soundName
                                                          ofType:fileExtension];
    SystemSoundID soundID;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:soundPath], &soundID);
    AudioServicesPlaySystemSound(soundID);
}


#pragma mark - Reduce Methods

- (void) object:(UIView *)currentObject withBorderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth cornerRadius:(CGFloat)cornerRadius shadowOpacity:(CGFloat)shadowOpacity shadowColor:(UIColor *)shadowColor {
    
    currentObject.layer.borderColor = borderColor.CGColor;
    currentObject.layer.borderWidth = borderWidth;
    currentObject.layer.cornerRadius = cornerRadius;
    currentObject.layer.shadowOpacity = shadowOpacity;
    currentObject.layer.shadowColor = shadowColor.CGColor;
}

- (CGFloat) createValueFromNumbers:(NSInteger)number {
    
    NSString *tmpNumberString = [NSString stringWithFormat:@"%li", number];
    self.totalValueLabel.text = [self.totalValueLabel.text stringByAppendingString:tmpNumberString];
    self.totalValueLabel.text = (self.totalValueLabel.text.length > 10) ?
    [self.totalValueLabel.text substringToIndex:10] : self.totalValueLabel.text;
    
    return [self.totalValueLabel.text doubleValue];
}

- (void) nextOperation {
    
    self.totalValueLabel.text = [self calculate:self.currentTag];
    
    self.firstValue = [self numberFromString:self.totalValueLabel.text];
    self.secondValue = 0;
    
    self.stateOfClearSecondValue = ABBeginClearSecondValue;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
