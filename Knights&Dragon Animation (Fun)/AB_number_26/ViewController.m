//
//  ViewController.m
//  AB_number_26
//
//  Created by Alexander Berezovskyy on 28.03.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ViewController.h"
#import <AudioToolbox/AudioToolbox.h>

typedef enum {
    
    ABWeatherSun,
    ABWeatherCloud,
    ABWeatherRain
    
}ABWeather;

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *firstCloud;
@property (weak, nonatomic) IBOutlet UIImageView *secondCloud;

@property (strong, nonatomic) NSMutableArray *dropsArray;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *starsCollection;
@property (weak, nonatomic) IBOutlet UIImageView *sunAndMoonImageView;
@property (weak, nonatomic) IBOutlet UIView *magicWorldView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *weatherSegmentedControl;   // nature
@property (weak, nonatomic) IBOutlet UISlider *timeSlider;                          // day-night

@property (strong, nonatomic) UIImageView *lightningView;                           // lightning
@property (strong, nonatomic) UIImageView *dragonImageView;                         // dragon
@property (strong, nonatomic) UIImageView *firstKnightImageView;                    // Knight №1

// Animations arrays

@property (strong, nonatomic) NSArray *horseArray;
@property (strong, nonatomic) NSArray *firstKnightArray;
@property (strong, nonatomic) NSArray *secondKnightRightArray;
@property (strong, nonatomic) NSArray *secondKnightLeftArray;
@property (strong, nonatomic) NSArray *dragonLeftArray;
@property (strong, nonatomic) NSArray *dragonRightArray;


- (IBAction)partOfDayActionSlider:(UISlider *)sender;
- (IBAction)weatherSegmentedAction:(UISegmentedControl *)sender;
- (IBAction)lightningAction:(UIButton *)sender;
- (IBAction)dragonSwitchAction:(UISwitch *)sender;
- (IBAction)firstKnightSwitchAction:(UISwitch *)sender;
- (IBAction)secondKnightSwitchAction:(UISwitch *)sender;

- (void) changeColorOfSky;
- (void) createStarsWithColor:(UIColor *)starColor;
- (void) changeSunAndMoon;
- (void) natureEvents;
- (void) rain;
- (void) playSoundWithName:(NSString *)soundName andExtension:(NSString *)fileExtension;

@end

@implementation ViewController


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Set color of magicWorldView
    
    [self changeColorOfSky];
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Create stars
    
    for (UIView *star in self.starsCollection) {
        
        star.backgroundColor = [UIColor clearColor];
        star.layer.cornerRadius = star.bounds.size.height;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Initialization dropsArray
    
    self.dropsArray = [NSMutableArray new];
    
    // Create dragon amimation (Right)
    
    self.dragonImageView = [[UIImageView alloc] initWithFrame:CGRectMake(-150, CGRectGetHeight(self.magicWorldView.bounds)/3.5f, CGRectGetWidth(self.magicWorldView.bounds)/3, CGRectGetHeight(self.magicWorldView.bounds)/3)];
    [self.view addSubview:self.dragonImageView];
    
    UIImage *dragonImage1 = [UIImage imageNamed:@"dragon_2_1.png"];
    UIImage *dragonImage2 = [UIImage imageNamed:@"dragon_2_2.png"];
    UIImage *dragonImage3 = [UIImage imageNamed:@"dragon_2_3.png"];
    UIImage *dragonImage4 = [UIImage imageNamed:@"dragon_2_4.png"];
    UIImage *dragonImage5 = [UIImage imageNamed:@"dragon_2_5.png"];
    UIImage *dragonImage6 = [UIImage imageNamed:@"dragon_2_6.png"];
    UIImage *dragonImage7 = [UIImage imageNamed:@"dragon_2_7.png"];
    UIImage *dragonImage8 = [UIImage imageNamed:@"dragon_2_8.png"];
    
    self.dragonRightArray = [[NSArray alloc] initWithObjects:dragonImage1, dragonImage2, dragonImage3, dragonImage4, dragonImage5, dragonImage6, dragonImage7, dragonImage8, nil];
    
    self.dragonImageView.animationImages = self.dragonRightArray;
    
    // Create dragon amimation (Right)
    
    UIImage *dragonImageLeft1 = [UIImage imageNamed:@"dragon_2_1_left.png"];
    UIImage *dragonImageLeft2 = [UIImage imageNamed:@"dragon_2_2_left.png"];
    UIImage *dragonImageLeft3 = [UIImage imageNamed:@"dragon_2_3_left.png"];
    UIImage *dragonImageLeft4 = [UIImage imageNamed:@"dragon_2_4_left.png"];
    UIImage *dragonImageLeft5 = [UIImage imageNamed:@"dragon_2_5_left.png"];
    UIImage *dragonImageLeft6 = [UIImage imageNamed:@"dragon_2_6_left.png"];
    UIImage *dragonImageLeft7 = [UIImage imageNamed:@"dragon_2_7_left.png"];
    UIImage *dragonImageLeft8 = [UIImage imageNamed:@"dragon_2_8_left.png"];
    
    self.dragonLeftArray = [[NSArray alloc] initWithObjects:dragonImageLeft1, dragonImageLeft2, dragonImageLeft3, dragonImageLeft4, dragonImageLeft5, dragonImageLeft6, dragonImageLeft7, dragonImageLeft8, nil];
    
    // Create Knight №1 amimation
    
    self.firstKnightImageView = [[UIImageView alloc] initWithFrame:CGRectMake(-150, CGRectGetHeight(self.magicWorldView.bounds) - CGRectGetHeight(self.magicWorldView.bounds)/5.f, CGRectGetWidth(self.magicWorldView.bounds)/3.f, CGRectGetHeight(self.magicWorldView.bounds)/5.f)];
    
    [self.view addSubview:self.firstKnightImageView];
    
    UIImage *firstKnightImage1 = [UIImage imageNamed:@"knight_3_1.png"];
    UIImage *firstKnightImage2 = [UIImage imageNamed:@"knight_3_2.png"];
    UIImage *firstKnightImage3 = [UIImage imageNamed:@"knight_3_3.png"];
    UIImage *firstKnightImage4 = [UIImage imageNamed:@"knight_3_4.png"];
    UIImage *firstKnightImage5 = [UIImage imageNamed:@"knight_3_5.png"];
    UIImage *firstKnightImage6 = [UIImage imageNamed:@"knight_3_6.png"];
    UIImage *firstKnightImage7 = [UIImage imageNamed:@"knight_3_7.png"];
    UIImage *firstKnightImage8 = [UIImage imageNamed:@"knight_3_8.png"];
    UIImage *firstKnightImage9 = [UIImage imageNamed:@"knight_3_9.png"];
    
    self.firstKnightArray = [[NSArray alloc] initWithObjects:firstKnightImage1, firstKnightImage2, firstKnightImage3, firstKnightImage4, firstKnightImage5, firstKnightImage6, firstKnightImage7, firstKnightImage8, firstKnightImage9, nil];
    
    // Create Knight №2 amimation (Right)
    
    UIImage *secondKnightRightImage1 = [UIImage imageNamed:@"knight_2_1_right.png"];
    UIImage *secondKnightRightImage2 = [UIImage imageNamed:@"knight_2_2_right.png"];
    
    self.secondKnightRightArray = [[NSArray alloc] initWithObjects:secondKnightRightImage1, secondKnightRightImage2, nil];
    
    // Create Knight №2 amimation (Left)
    
    UIImage *secondKnightLeftImage1 = [UIImage imageNamed:@"knight_2_1_left.png"];
    UIImage *secondKnightLeftImage2 = [UIImage imageNamed:@"knight_2_2_left.png"];
    
    self.secondKnightLeftArray = [[NSArray alloc] initWithObjects:secondKnightLeftImage1, secondKnightLeftImage2, nil];
    
    // Create Horse amimation
    
    UIImage *horseImage1 = [UIImage imageNamed:@"horse_1_1.png"];
    UIImage *horseImage2 = [UIImage imageNamed:@"horse_1_2.png"];
    UIImage *horseImage3 = [UIImage imageNamed:@"horse_1_3.png"];
    UIImage *horseImage4 = [UIImage imageNamed:@"horse_1_4.png"];
    UIImage *horseImage5 = [UIImage imageNamed:@"horse_1_5.png"];
    UIImage *horseImage6 = [UIImage imageNamed:@"horse_1_6.png"];
    UIImage *horseImage7 = [UIImage imageNamed:@"horse_1_7.png"];
    UIImage *horseImage8 = [UIImage imageNamed:@"horse_1_8.png"];
    UIImage *horseImage9 = [UIImage imageNamed:@"horse_1_9.png"];
    UIImage *horseImage10 = [UIImage imageNamed:@"horse_1_10.png"];
    
    self.horseArray = [[NSArray alloc] initWithObjects:horseImage1, horseImage2, horseImage3, horseImage4, horseImage5, horseImage6, horseImage7, horseImage8, horseImage9, horseImage10, nil];
    
    // Create lightning
    
    self.lightningView = [[UIImageView alloc]
                          initWithFrame: CGRectMake(CGRectGetWidth(self.magicWorldView.bounds)/6.8f, 0, CGRectGetWidth(self.magicWorldView.bounds)* 0.7f, CGRectGetHeight(self.magicWorldView.bounds) * 0.9f)];
    
    self.lightningView.image = [UIImage imageNamed:@"lightning_2.png"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Actions

- (IBAction)partOfDayActionSlider:(UISlider *)sender {
    
    // Change color of sky

    [self changeColorOfSky];
    
    // Day
    
    if (self.timeSlider.value >= 100) {
        
        [self createStarsWithColor:[UIColor colorWithRed:212/255.f green:1 blue:1 alpha:1]];
        self.sunAndMoonImageView.image = [UIImage imageNamed:@"moon.png"];
        
    // Night
        
    } else {
        
        [self createStarsWithColor:[UIColor clearColor]];
        self.sunAndMoonImageView.image = [UIImage imageNamed:@"sun_2.png"];
    }
    
    [self changeSunAndMoon];
    
    self.firstCloud.alpha = self.secondCloud.alpha = 1 - self.timeSlider.value/400.f;
}

- (IBAction)weatherSegmentedAction:(UISegmentedControl *)sender {
    [self natureEvents];
}

- (IBAction)lightningAction:(UIButton *)sender {
    
    [self playSoundWithName:@"thunder_1" andExtension:@"wav"];  //sound

    [self.view addSubview:self.lightningView];
    
    UIColor *tmpColor = self.magicWorldView.backgroundColor;
    
    [UIView animateWithDuration:0.2f
     animations:^{
         self.magicWorldView.backgroundColor = [UIColor whiteColor];
     }
     completion:^(BOOL finished) {
         
         self.magicWorldView.backgroundColor = tmpColor;
         
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
             [self.lightningView removeFromSuperview];
         });
     }];
}

- (IBAction)dragonSwitchAction:(UISwitch *)sender {
    
    [self playSoundWithName:@"dragon_2" andExtension:@"mp3"];  //sound
    
    if (sender.isOn) {
        
        [UIView animateWithDuration:7.f
         animations:^{
             
             self.dragonImageView.animationDuration = 0.7f;
             [self.dragonImageView startAnimating];
             
             self.dragonImageView.center = CGPointMake(CGRectGetWidth(self.magicWorldView.bounds) + 200, CGRectGetHeight(self.magicWorldView.bounds)/3.5f + CGRectGetHeight(self.dragonImageView.bounds)/2);
             
         }
         completion:^(BOOL finished) {
             
             self.dragonImageView.center = CGPointMake(CGRectGetMinX (self.magicWorldView.bounds) - 150, CGRectGetHeight(self.magicWorldView.bounds)/3.5f + CGRectGetHeight(self.dragonImageView.bounds)/2);
             
             [sender setOn:NO];
             
             [self.dragonImageView stopAnimating];
         }];
    }
}

- (IBAction)firstKnightSwitchAction:(UISwitch *)sender {
    
    [self playSoundWithName:@"horse_1" andExtension:@"mp3"];  //sound
    
    if (sender.isOn) {
        
        [UIView animateWithDuration:7.f
         animations:^{
             
             self.firstKnightImageView.animationImages = self.firstKnightArray;
             self.firstKnightImageView.animationDuration = 0.8f;
             [self.firstKnightImageView startAnimating];
             
             self.firstKnightImageView.center = CGPointMake(CGRectGetWidth(self.magicWorldView.bounds) + 200, CGRectGetHeight(self.magicWorldView.bounds) - CGRectGetHeight(self.firstKnightImageView.bounds)/2);
             
         }
         completion:^(BOOL finished) {
             
             [self playSoundWithName:@"horse_1" andExtension:@"mp3"];  //sound
             
             self.firstKnightImageView.animationImages = self.horseArray;
             self.firstKnightImageView.animationDuration = 0.7f;
             [self.firstKnightImageView startAnimating];
             
             [UIView animateWithDuration:4.f
              animations:^{
                  
               self.firstKnightImageView.center = CGPointMake(-150, CGRectGetHeight(self.magicWorldView.bounds) - CGRectGetHeight(self.firstKnightImageView.bounds)/2);
                  
              }
              completion:^(BOOL finished) {
                  
                  self.firstKnightImageView.animationImages = self.firstKnightArray;
                  
                  [sender setOn:NO];
                  
                  [self.firstKnightImageView stopAnimating];
              }];
         }];
    }
}

- (IBAction)secondKnightSwitchAction:(UISwitch *)sender {
    
    [self playSoundWithName:@"kiss_3" andExtension:@"mp3"];  //sound
    
     self.dragonImageView.center = CGPointMake(CGRectGetWidth(self.magicWorldView.bounds) + 100, CGRectGetHeight(self.magicWorldView.bounds) - CGRectGetHeight(self.dragonImageView.bounds)/2);
    
    self.dragonImageView.animationImages = self.dragonLeftArray;
    self.dragonImageView.animationDuration = 0.4f;
    [self.dragonImageView startAnimating];
    
    [UIView animateWithDuration:6.f
     animations:^{
         
         self.firstKnightImageView.animationImages = self.secondKnightRightArray;
         self.firstKnightImageView.animationDuration = 0.3f;
         [self.firstKnightImageView startAnimating];
         
         self.firstKnightImageView.center = CGPointMake(CGRectGetWidth(self.magicWorldView.bounds) + 200, CGRectGetHeight(self.magicWorldView.bounds) - CGRectGetHeight(self.firstKnightImageView.bounds)/2);
         
     }
     completion:^(BOOL finished) {
         
     self.firstKnightImageView.animationImages = self.secondKnightLeftArray;
     self.firstKnightImageView.animationDuration = 0.3f;
     [self.firstKnightImageView startAnimating];
     
         [UIView animateWithDuration:6.f
          animations:^{
              
              self.firstKnightImageView.center = CGPointMake(-150, CGRectGetHeight(self.magicWorldView.bounds) - CGRectGetHeight(self.firstKnightImageView.bounds)/2);
              
              self.dragonImageView.center = CGPointMake(CGRectGetMinX (self.magicWorldView.bounds) - 500, CGRectGetHeight(self.magicWorldView.bounds) - CGRectGetHeight(self.dragonImageView.bounds)/2);
              
          }
          completion:^(BOOL finished) {
        
              self.firstKnightImageView.animationImages = self.firstKnightArray;
              self.dragonImageView.animationImages = self.dragonRightArray;
              
             self.dragonImageView.center = CGPointMake(CGRectGetMinX (self.magicWorldView.bounds) - 150, CGRectGetHeight(self.magicWorldView.bounds)/3.5f + CGRectGetHeight(self.dragonImageView.bounds)/2);
              
              [sender setOn:NO];
              
              [self.firstKnightImageView stopAnimating];
              [self.dragonImageView stopAnimating];
              }];
     }];
}


#pragma mark - Nature Methods

- (void) changeColorOfSky {
    
    CGFloat colorValue = self.timeSlider.value/255.f;
    
    self.magicWorldView.backgroundColor = [UIColor colorWithRed:212/255.f - colorValue
                                                          green:1 - colorValue
                                                           blue:1 - colorValue
                                                          alpha:1.f];
}

- (void) createStarsWithColor:(UIColor *)starColor {
    
    for (UIView *star in self.starsCollection) {
        star.backgroundColor = starColor;
        star.layer.cornerRadius = star.bounds.size.height * 1.2f;
        star.layer.shadowOpacity = 1.f;
        star.layer.shadowRadius = self.timeSlider.value / 18.f;
        star.layer.shadowColor = [UIColor whiteColor].CGColor;
        star.layer.shadowOffset = CGSizeMake(0, 0);
    }
}

- (void) changeSunAndMoon {
    
    if (self.timeSlider.value <= 100) {
        
        CGFloat tmpValue = 1 - self.timeSlider.value/100.f;
        self.sunAndMoonImageView.transform = CGAffineTransformMakeScale(tmpValue, tmpValue);

    } else {
        
        CGFloat tmpValue = self.timeSlider.value/100.f - 1;
        self.sunAndMoonImageView.transform = CGAffineTransformMakeScale(tmpValue, tmpValue);
        
        for (UIView *star in self.starsCollection) {
            star.transform = CGAffineTransformMakeScale(tmpValue + 0.2f, tmpValue + 0.2f);
        }
    }
}

- (void) natureEvents {
    
    if (self.weatherSegmentedControl.selectedSegmentIndex == ABWeatherSun) {
        
        [self playSoundWithName:@"nature_1" andExtension:@"mp3"];  //sound
        
        // Delete rain
        
        for (UIImageView *dropView in self.dropsArray) {
            
            if ([self.dropsArray count] != 0) {
                [dropView removeFromSuperview];
            }
        }
        
        [UIView animateWithDuration:1.3f animations:^{
            
            self.firstCloud.center = CGPointMake(-200,
                                                 CGRectGetMinY(self.firstCloud.frame) + CGRectGetHeight(self.firstCloud.bounds)/2);
            
            self.secondCloud.center = CGPointMake(CGRectGetWidth(self.view.bounds) + 200,
                                                  CGRectGetMinY(self.secondCloud.frame) + CGRectGetHeight(self.secondCloud.bounds)/2);
        }];
    
    } else if (self.weatherSegmentedControl.selectedSegmentIndex == ABWeatherCloud) {
        
        [self playSoundWithName:@"wind_2" andExtension:@"wav"];  //sound
        
        [UIView animateWithDuration:1.3f
         animations:^{
            
            self.firstCloud.center = CGPointMake(CGRectGetWidth(self.firstCloud.bounds) * 0.8f,
                                                 CGRectGetMinY(self.firstCloud.frame) + CGRectGetHeight(self.firstCloud.bounds)/2);
            
            self.secondCloud.center = CGPointMake(CGRectGetWidth(self.view.bounds)
                                                  - CGRectGetWidth(self.secondCloud.bounds) * 0.8f,
                                                  CGRectGetMinY(self.secondCloud.frame) + CGRectGetHeight(self.secondCloud.bounds)/2);
        }];
    
    } else if (self.weatherSegmentedControl.selectedSegmentIndex == ABWeatherRain) {
        
        [self playSoundWithName:@"rain_2" andExtension:@"mp3"];  //sound
   
        for (int i = 0; i < 500; i++) {
            [self rain];
        }
    }
}

- (void) rain {
    
    // Create drop
    
    UIImageView *dropImageView = [[UIImageView alloc] initWithFrame:CGRectMake(arc4random_uniform(self.magicWorldView.bounds.size.width), -20, 12, 12)];
    dropImageView.image = [UIImage imageNamed:@"drop.png"];
    [self.view addSubview:dropImageView];
    
    [self.dropsArray addObject:dropImageView];
    
    // Settings of rain type & speed
    
    CGFloat animeDuration = arc4random_uniform(100)/200.f + 1.f;
    NSInteger typeOfRain = arc4random_uniform(4);
    
    [UIView animateWithDuration:animeDuration
                          delay:arc4random_uniform(500) / 50.f
                        options:typeOfRain
     animations:^{
         
         dropImageView.center = CGPointMake(CGRectGetMidX(dropImageView.frame),
                                            CGRectGetMaxY(self.magicWorldView.bounds) - arc4random_uniform(10));
     }
     completion:^(BOOL finished) {
     }];
}

#pragma mark - Sounds

- (void) playSoundWithName:(NSString *)soundName andExtension:(NSString *)fileExtension {
    
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:soundName
                                                          ofType:fileExtension];
    SystemSoundID soundID;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:soundPath], &soundID);
    AudioServicesPlaySystemSound(soundID);
}



@end
