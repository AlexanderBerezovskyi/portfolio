//
//  ABMusicVC.m
//  AB_number_27-28
//
//  Created by Alexander Berezovskyy on 13.04.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABMusicVC.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

typedef enum {
    
    ABRadioCurrentSegment,
    ABMyMusicCurrentSegment
    
}ABCurrentSegment;

@interface ABMusicVC ()

// audio

@property (nonatomic, strong) AVPlayer *radioPlayer;
@property (nonatomic, strong) AVPlayerItem *radioPlayerItem;
@property(nonatomic, strong) AVAudioPlayer *savedMusic;

// Views

@property (weak, nonatomic) IBOutlet UISwitch *soundOnOffSwitch;
@property (weak, nonatomic) IBOutlet UISlider *volumeValueSlider;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sourceSoundSegment;
@property (weak, nonatomic) IBOutlet UILabel *trackDetails;

@property (nonatomic, assign) Boolean observerFlag;

@end

static NSString *kSettingsVolume      = @"volume";
static NSString *kSettingsSourceSound = @"sourceSound";


@implementation ABMusicVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // set Table View background
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.jpg"]];
    
    [self loadSettings];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)volumeChangedAction:(UISlider *)sender {
    
    [self refreshVolume];
}

- (IBAction)musicAction:(id)sender {
    
    if ([self.soundOnOffSwitch isOn]) {
        
        if (self.sourceSoundSegment.selectedSegmentIndex == ABRadioCurrentSegment) {
        
        [self.savedMusic pause];

            [self playRadioWithURL:@"http://64.202.98.133:2010"];
            
        } else if (self.sourceSoundSegment.selectedSegmentIndex == ABMyMusicCurrentSegment) {
       
            [self.radioPlayer pause];
            [self playSavedMusic:@"phantom.mp3" repeat:YES];
            [self deleteObserver];
        }
        
    } else {
        
        [self.savedMusic stop];
        [self.radioPlayer pause];
        [self deleteObserver];
    }
    
    [self saveSettings];
}

#pragma mark - Music settings

- (void) refreshVolume {

    self.savedMusic.volume = self.volumeValueSlider.value;
    self.radioPlayer.volume = self.volumeValueSlider.value;
    
    [self saveSettings];
}

- (void) playRadioWithURL:(NSString *)radioURL {
    
    self.radioPlayerItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:radioURL]];
    
    [self.radioPlayerItem addObserver:self forKeyPath:@"timedMetadata" options:NSKeyValueObservingOptionNew context:nil];
    self.observerFlag = YES;
    
    self.radioPlayer = [AVPlayer playerWithPlayerItem:self.radioPlayerItem];

    self.radioPlayer.volume = self.volumeValueSlider.value;
    
    [self.radioPlayer play];
}

- (void) playSavedMusic:(NSString *)currentTrack repeat:(Boolean)repeat {
    
    NSURL *musicFile = [[NSBundle mainBundle] URLForResource:currentTrack
                                               withExtension:nil];
    
    self.savedMusic = [[AVAudioPlayer alloc] initWithContentsOfURL:musicFile
                                                             error:nil];
    if (repeat) {
        self.savedMusic.numberOfLoops = -1;
    } else {
        self.savedMusic.numberOfLoops = 0;
    }
    
    self.savedMusic.volume = self.volumeValueSlider.value;
    [self.savedMusic play];
}


#pragma mark - Observer

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    if ([keyPath isEqualToString:@"timedMetadata"]) {
        
        AVPlayerItem* _playerItem = object;
        
        for (AVMetadataItem *metadata in _playerItem.timedMetadata) {
            
            if ([[metadata.key description] isEqualToString:@"title"]) {
                self.trackDetails.text = [NSString stringWithFormat:@"%@", metadata.stringValue];
            }
        }
    }
}

- (void) deleteObserver {
    
    if (self.observerFlag == YES) {
        [self.radioPlayerItem removeObserver:self forKeyPath:@"timedMetadata" context:nil];
        self.observerFlag = NO;
    }
}


#pragma mark - Navigation VC

- (void) viewWillDisappear:(BOOL)animated {
    
    [self deleteObserver];
}


#pragma mark - Save and Load

- (void) saveSettings {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setFloat:self.volumeValueSlider.value forKey:kSettingsVolume];
    [userDefaults setInteger:self.sourceSoundSegment.selectedSegmentIndex forKey:kSettingsSourceSound];
    
    [userDefaults synchronize];
}

- (void) loadSettings {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.volumeValueSlider.value = [userDefaults floatForKey:kSettingsVolume];
    self.sourceSoundSegment.selectedSegmentIndex = [userDefaults integerForKey:kSettingsSourceSound];
}

#pragma mark - UITableViewDelegate

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return NO;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.backgroundColor = [UIColor clearColor];
    
}

- (void) tableView:(UITableView *)tableView willDisplayHeaderView:(nonnull UIView *)view forSection:(NSInteger)section {
    
    view.tintColor = [UIColor blackColor];
    
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor whiteColor]];
    header.alpha = 0.4f;
}



@end
