//
//  ABRegistrationTableViewController.m
//  AB_number_27-28
//
//  Created by Alexander Berezovskyy on 12.04.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABRegistrationTableViewController.h"
#import "ABSuccessRegistrationVC.h"

typedef enum {
    
    ABTextFieldLogin = 1,
    ABTextFieldPassword,
    ABTextFieldFirstName,
    ABTextFieldLastName,
    ABTextFieldAge,
    ABTextFieldEmail,
    ABTextFieldPhone
    
}ABTextField;

typedef enum {
    
    ABLittleLabelLogin,
    ABLittleLabelPassword,
    ABLittleLabelFirstName,
    ABLittleLabelLastName,
    ABLittleLabelAge,
    ABLittleLabelEmail,
    ABLittleLabelPhone
    
}ABLittleLabel;

// Set number phone format

static const int localNumberMaxLength = 7;
static const int areaCodeMaxLength = 3;
static const int countryCodeMaxLength = 3;
static const int notDecimalSymbolsCountryPhoneFormat = 6;
static const int notDecimalSymbolsLocalPhoneFormat = 1;


@interface ABRegistrationTableViewController ()

@property (strong, nonatomic) NSString *privateInfo;

// Text fields

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;

// Other buttons

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *clearAllFieldsButton;
@property (weak, nonatomic) IBOutlet UISwitch *rememberSwitch;

// Arrays
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *informationTextFieldCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *littleLabelCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *bigLabelCollection;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *otherButtonCollection;

// Edit part after "@" in email

@property (assign, nonatomic) NSInteger atIndex;
@property (strong, nonatomic) NSString *stringBeforeAt;
@property (strong, nonatomic) NSString *stringAfterAt;

// save regitration data

@property (strong, nonatomic) NSString *registrationData;

@end

static NSString *kRegistrationLogin     = @"login";
static NSString *kRegistrationPassword  = @"password";
static NSString *kRegistrationFirstName = @"firstName";
static NSString *kRegistrationLastName  = @"lastName";
static NSString *kRegistrationAge       = @"age";
static NSString *kRegistrationEmail     = @"email";
static NSString *kRegistrationPhone     = @"phone";
static NSString *kRegistrationRemember  = @"remember";
static NSString *kEmailPartAfterAt      = @"beforeAt";

@implementation ABRegistrationTableViewController

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.atIndex = 0;
        self.stringBeforeAt = @"";
        self.stringAfterAt = @"";
        self.registrationData = @"";
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Navigation Bar settings
    
    [self.navigationController.navigationBar
     setBackgroundImage:[UIImage imageNamed:@"cell_4.jpg"]
     forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.alpha = 0.2f;

    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"TrebuchetMS-Italic" size:25.f]}];
    
    // set Table View background

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cell_4.jpg"]];
    
    // settings clear button and submit button
    
    for (UIButton *button in self.otherButtonCollection) {
        
        [self view:button
   setCornerRadius:5.f
       borderWidth:1.5f
       borderColor:[UIColor cyanColor]
     shadowOpacity:1.f
       shadowColor:[UIColor whiteColor]
         tintColor:[UIColor whiteColor]];
    }
    
    // settings labels
    
    for (UILabel *bigLabel in self.bigLabelCollection) {
        bigLabel.textColor = [UIColor whiteColor];
    }
    
    for (UILabel *littleLabel in self.littleLabelCollection) {
        littleLabel.textColor = [UIColor whiteColor];
        littleLabel.text = @"";
    }
    
    // settings text fields
    
    for (UITextField *infoField in self.informationTextFieldCollection) {
        
        [self view:infoField
   setCornerRadius:5.f
       borderWidth:0.3f
       borderColor:[UIColor cyanColor]
     shadowOpacity:0.5f
       shadowColor:[UIColor purpleColor]
         tintColor:[UIColor blackColor]];
        
    }
    
    [[self.informationTextFieldCollection firstObject] becomeFirstResponder];
    
    [self.submitButton setEnabled:NO];
    
    [self loadStateRememberSwitch];
    
    
    if ([self.rememberSwitch isOn]) {
        
        [self loadRegistrationTextFieldData];
        
    } else {
        
        [self clearAll];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField.text isEqualToString:@""] ||
        ([textField.text characterAtIndex:textField.text.length - 1] == ' ')) {
        
        if ([string isEqualToString:@" "]) {
            return NO;
        }
    }
    
    UILabel *currentLittleLabel = [self.littleLabelCollection objectAtIndex:textField.tag - 1];
    
    switch (textField.tag) {
            
        case ABTextFieldLogin:
        case ABTextFieldFirstName:
        case ABTextFieldLastName:
            
            if (![textField.text isEqualToString:@""] && (
                 [textField.text characterAtIndex:textField.text.length - 1] == '-' ||
                 [textField.text characterAtIndex:textField.text.length - 1] == ' ')) {
                
                if ([string isEqualToString:@"-"] || [string isEqualToString:@" "]) {
                    return NO;
                }
            }
            
            if ([textField.text isEqualToString:@""] && ([string isEqualToString:@"-"])) {
                return NO;
            }
            
            break;
            
        case ABTextFieldEmail:
            
            if (textField.tag == ABTextFieldEmail) {
                
                if ([string isEqualToString:@" "]) {
                    return NO;
                }
                
                if (textField.text.length == 0 && [string isEqualToString:@"@"]) {
                    
                    [self littleLable:currentLittleLabel
                              setText:@"At first character \"@\""
                            withColor:[UIColor yellowColor]];
                    
                    return NO;
                }
                
                if (textField.text.length != 0 && [textField.text containsString:@"@"])  {
                    
                    if ([string isEqualToString:@"@"]) {
                        
                        [self littleLable:currentLittleLabel
                                  setText:@"Too many \"@\""
                                withColor:[UIColor yellowColor]];
                        
                        return NO;
                    }
                }
                
                if ([textField.text characterAtIndex:textField.text.length - 1] == '.') {
                    
                    if ([string isEqualToString:@"."] || [string isEqualToString:@"@"]) {
                        
                        [self littleLable:currentLittleLabel
                                  setText:[string isEqualToString:@"."] ?
                         @"Too many dots in a row" : @"Dot can't be placed before \"@\""
                                withColor:[UIColor yellowColor]];
                        
                        return NO;
                    }
                }
                
                if ([textField.text characterAtIndex:textField.text.length - 1] == '@'){
                    
                    if ([string isEqualToString:@"."]) {
                        
                        [self littleLable:currentLittleLabel
                                  setText:@"Dot can't be placed after \"@\""
                                withColor:[UIColor yellowColor]];
                        
                        return NO;
                    }
                }
            }
            
            break;
            
        case ABTextFieldPhone:
            
            if ([textField.text length] > localNumberMaxLength + areaCodeMaxLength + countryCodeMaxLength + notDecimalSymbolsCountryPhoneFormat) {
                return NO;
            }
            
            break;
            
        default:
            
//                        NSLog(@"Other tag 2");
            
            break;
    }
    
    currentLittleLabel.textColor = [UIColor whiteColor];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    UITextField *currentTextField = [self.informationTextFieldCollection objectAtIndex:textField.tag - 1];
    UILabel *currentLittleLabel = [self.littleLabelCollection objectAtIndex:textField.tag - 1];
    
    switch (textField.tag) {
            
        case ABTextFieldLogin:
        case ABTextFieldFirstName:
        case ABTextFieldLastName:
            
            if (textField.text.length != 0 &&
                [textField.text characterAtIndex:textField.text.length - 1] != ' ' &&
                [textField.text characterAtIndex:textField.text.length - 1] != '-') {
                
                [self littleLable:currentLittleLabel
                          setText:@"Success!"
                        withColor:[UIColor cyanColor]];
                
            } else if ([textField.text characterAtIndex:textField.text.length - 1] == ' ' ||
                       [textField.text characterAtIndex:textField.text.length - 1] == '-') {
                
                [self littleLable:currentLittleLabel
                          setText:[NSString stringWithFormat:
                                   @"\"%@\" character can't be the last in a row",
                                   [textField.text characterAtIndex:textField.text.length - 1] == '-' ?
                                   @"-" : @"Space"]
                        withColor:[UIColor yellowColor]];
            }
            
            break;
            
        case ABTextFieldPassword:
            
            if ([textField.text characterAtIndex:textField.text.length - 1] == ' ') {
                
                [self littleLable:currentLittleLabel
                          setText:@"\"Space\" character can't be the last in a row"
                        withColor:[UIColor yellowColor]];
            }
            
            if (textField.text.length >= 6 && [textField.text characterAtIndex:textField.text.length - 1] != ' ') {
                
                [self littleLable:currentLittleLabel
                          setText:@"Success!"
                        withColor:[UIColor cyanColor]];
                
            } else if (textField.text.length < 6 && textField.text.length != 0) {
                
                [self littleLable:currentLittleLabel
                          setText:@"The password too short"
                        withColor:[UIColor yellowColor]];
            }
            
            break;
            
        case ABTextFieldAge:
            
            if ([currentTextField.text integerValue] < 18 &&
                [currentTextField.text integerValue] > 0) {
                
                [self littleLable:currentLittleLabel
                          setText:@"You must be at least 18 to use this app!"
                        withColor:[UIColor yellowColor]];
                
            } else if ([currentTextField.text integerValue] >= 18 &&
                       [currentTextField.text integerValue] <= 120) {
                
                [self littleLable:currentLittleLabel
                          setText:@"Success!"
                        withColor:[UIColor cyanColor]];
                
            } else if (![currentTextField.text isEqualToString:@""]) {
                
                [self littleLable:currentLittleLabel
                          setText:@"Incorrect value!"
                        withColor:[UIColor yellowColor]];
            }
            
            break;
            
        case ABTextFieldEmail:
            
            if (textField.text.length != 0 && (![textField.text containsString:@"@"] ||
                                               ![textField.text containsString:@"."])) {
                
                [self littleLable:currentLittleLabel
                          setText:[NSString stringWithFormat:@"Email must have \"%@\"",
                                   [textField.text containsString:@"." ] ? @"@" : @"."]
                        withColor:[UIColor yellowColor]];
            }
            
            if (textField.text.length != 0 &&
                ([textField.text characterAtIndex:textField.text.length - 1] == '@' ||
                 [textField.text characterAtIndex:textField.text.length - 1] == '.' )) {
                    
                    [self littleLable:currentLittleLabel
                              setText:[NSString stringWithFormat:
                                       @"\"%@\" character can't be the last in a row",
                                       [textField.text characterAtIndex:textField.text.length - 1] == '.' ?
                                       @"." : @"@"]
                            withColor:[UIColor yellowColor]];
                }
            
            if (![self.stringAfterAt containsString:@"."] &&
                ![self.stringAfterAt isEqualToString:@""] &&
                ![currentTextField.text isEqualToString:@""]) {
                
                [self littleLable:currentLittleLabel
                          setText:@"The part after \"@\" must have dot"
                        withColor:[UIColor yellowColor]];
            }
            
            if ([textField.text characterAtIndex:textField.text.length - 1] == '.') {
                
                [self littleLable:currentLittleLabel
                          setText:@"Dot can't be the last in a row"
                        withColor:[UIColor yellowColor]];
            }
            
            if (textField.text.length >= 5 && ![currentLittleLabel.textColor isEqual:[UIColor yellowColor]] ) {
                
                [self littleLable:currentLittleLabel
                          setText:@"Success!"
                        withColor:[UIColor cyanColor]];
            }
            
            break;
            
        case ABTextFieldPhone:
            
            if (textField.text.length >= localNumberMaxLength + notDecimalSymbolsLocalPhoneFormat) {
                
                [self littleLable:currentLittleLabel
                          setText:@"Success!"
                        withColor:[UIColor cyanColor]];
                
            } else if ((textField.text.length < localNumberMaxLength + notDecimalSymbolsLocalPhoneFormat) &&
                       ![textField.text isEqualToString:@""])  {
                
                [self littleLable:currentLittleLabel
                          setText:@"The phone number too short"
                        withColor:[UIColor yellowColor]];
            } else {
                
                [self littleLable:currentLittleLabel
                          setText:@""
                        withColor:[UIColor yellowColor]];
            }
            
            break;
            
        default:
            
            NSLog(@"Other tag #1");
            
            break;
    }
    
    ([self countLittleCyanLabel] == [self.littleLabelCollection count]) ?
    [self.submitButton setEnabled:YES] : [self.submitButton setEnabled:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSInteger currentTextFieldIndex = [self.informationTextFieldCollection indexOfObject:textField];
    
    UILabel *currentLittleLabel = [self.littleLabelCollection objectAtIndex:textField.tag - 1];
    
    if ([self countLittleCyanLabel] != [self.littleLabelCollection count] ||
        ([self countLittleCyanLabel] != ([self.littleLabelCollection count] - 1) &&
         [currentLittleLabel.textColor isEqual:[UIColor whiteColor]])){
            
            for (int i = ((int)currentTextFieldIndex + 1); i < [self.informationTextFieldCollection count]; i++) {
                
                [self nextFieldForChangeFromIndex:i];
                
                UILabel *currentLittleLabelNew = [self.littleLabelCollection objectAtIndex:i];
                
                if (![currentLittleLabelNew.textColor isEqual:[UIColor cyanColor]]) {
                    
                    [self nextFieldForChangeFromIndex:i];
                    
                    break;
                    
                }
                else if (i == [self.informationTextFieldCollection count] - 1) {
                    
                    for (int j = 0; j < (int)currentTextFieldIndex + 1; j++) {
                        
                        UILabel *currentLittleLabelNew = [self.littleLabelCollection objectAtIndex:j];
                        
                        if (![currentLittleLabelNew.textColor isEqual:[UIColor cyanColor]]) {
                            
                            [self nextFieldForChangeFromIndex:j];
                            
                            break;
                        }
                    }
                }
            }
            
        } else {
            
            [textField resignFirstResponder];
        }
    
    if ([textField isEqual: [self.informationTextFieldCollection lastObject]]) {
        [[self.informationTextFieldCollection lastObject] resignFirstResponder];
    }
    
    return YES;
}


#pragma mark - Actions

- (IBAction)actionTextFieldChanged:(UITextField *)sender {
    
    UITextField *currentTextField = [self.informationTextFieldCollection objectAtIndex:sender.tag - 1];
    UILabel *currentLittleLabel = [self.littleLabelCollection objectAtIndex:sender.tag - 1];
    currentLittleLabel.textColor = [UIColor whiteColor];
    
    NSInteger maxLength = 0;
    
    switch (sender.tag) {
            
        case ABTextFieldLogin:
            
            maxLength = 20;
            
            currentTextField.text = [self textField:currentTextField validSet:[self unionCharacterSet:[NSCharacterSet alphanumericCharacterSet] withCharacters:@"- "]];
            break;
            
        case ABTextFieldPassword:
            
            maxLength = 25;
            
            break;
            
        case ABTextFieldFirstName:
        case ABTextFieldLastName:
            
            maxLength = 30;
            
            currentTextField.text = [self textField:currentTextField validSet:[self unionCharacterSet:[NSCharacterSet letterCharacterSet] withCharacters:@"- "]];
            
            break;
            
        case ABTextFieldAge:
            
            maxLength = 3;
            
            currentTextField.text = [self textField:currentTextField validSet:[NSCharacterSet decimalDigitCharacterSet]];
            
            break;
            
        case ABTextFieldEmail:
            
            maxLength = 40;
            
            if ([currentTextField.text containsString:@"@"]) {
                
                if ([currentTextField.text characterAtIndex:currentTextField.text.length - 1] == '@') {
                    self.atIndex = currentTextField.text.length - 1;
                    self.stringBeforeAt = [currentTextField.text substringToIndex:self.atIndex + 1];
                }
                
                self.stringAfterAt = [currentTextField.text substringFromIndex:
                                      self.atIndex + 1];
                
                NSMutableCharacterSet *tmpCharSet = [NSMutableCharacterSet alphanumericCharacterSet];
                
                [tmpCharSet addCharactersInString:@"-."];
                
                NSArray* charactersAfterAt = [self.stringAfterAt componentsSeparatedByCharactersInSet:[tmpCharSet invertedSet]];
                
                self.stringAfterAt = [charactersAfterAt componentsJoinedByString:@""];
                
                currentTextField.text = [self.stringBeforeAt stringByAppendingString:self.stringAfterAt];
            }
            
            break;
            
        case ABTextFieldPhone:
            
            currentTextField.text = [self textField:currentTextField validSet:[NSCharacterSet decimalDigitCharacterSet]];
            
            [self setPhoneNumberFormatForTextField:currentTextField];
            
            break;
            
        default:
            
            NSLog(@"Other sender");
            
            break;
    }
    
    if (sender.tag != ABTextFieldPhone) {
        
        [self changeTextField:currentTextField
                withMaxLength:maxLength
           andChangeLabelText:currentLittleLabel];
    }
}

- (IBAction)actionSubmitButton:(UIButton *)sender {
    
    self.registrationData = [NSString stringWithFormat:@"\n********************** \nDate of registration = %@,\n login = %@,\n pass = %@,\n FN = %@,\n LN = %@,\n age = %@,\n e-mail = %@,\n phone = %@\n%@", [NSDate date], self.loginTextField.text, self.passwordTextField.text, self.firstNameTextField.text, self.lastNameTextField.text, self.ageTextField.text, self.emailTextField.text, self.phoneTextField.text, [[NSUserDefaults standardUserDefaults] objectForKey:@"privateInfoAll"]];
    
//    NSLog(@"Date of registration = %@,\n login = %@,\n pass = %@,\n FN = %@,\n LN = %@,\n age = %@,\n e-mail = %@,\n phone = %@\n%@", [NSDate date], self.loginTextField.text, self.passwordTextField.text, self.firstNameTextField.text, self.lastNameTextField.text, self.ageTextField.text, self.emailTextField.text, self.phoneTextField.text, [[NSUserDefaults standardUserDefaults] objectForKey:@"privateInfoAll"]);
    
    [self saveRegistrationData];
    
    [self saveStateRememberSwitch];
    
    if ([self.rememberSwitch isOn]) {
        [self saveRegistrationTextFieldData];
    }
}

- (IBAction)actionClearAllButton:(UIButton *)sender {
    
    [self clearAll];
}


#pragma mark - UITextField settings

- (NSString *) textField:(UITextField *)currentTextField setLength:(NSInteger)textLength {
    
    currentTextField.text = (currentTextField.text.length > textLength) ?
    [currentTextField.text substringToIndex:textLength] : currentTextField.text;
    
    return currentTextField.text;
}

- (NSString *) textField:(UITextField *)currentTextField validSet:(NSCharacterSet *)currentSet {
    
    NSCharacterSet* validationSet = [currentSet invertedSet];
    NSArray* components = [currentTextField.text componentsSeparatedByCharactersInSet:validationSet];
    
    return [components componentsJoinedByString:@""];
}


#pragma mark - Reduce Methods

- (NSCharacterSet *) unionCharacterSet:(NSCharacterSet *)characterSet withCharacters:(NSString *)characters {
    
    NSMutableCharacterSet *mutCharSet = [NSMutableCharacterSet characterSetWithCharactersInString:characters];
    
    [mutCharSet formUnionWithCharacterSet:characterSet];
    
    NSCharacterSet *newCharacterSet = mutCharSet;
    
    return newCharacterSet;
}

- (void) littleLable:(UILabel *)littleLabel setText:(NSString *)labelText withColor:(UIColor *)labelTextColor {
    
    littleLabel.textColor = labelTextColor;
    littleLabel.text = labelText;
    
}

- (void) changeTextField:(UITextField *)textField withMaxLength:(NSInteger)maxLength andChangeLabelText:(UILabel *)label {
    
    NSInteger tmpStringLength = 0;
    
    textField.text = [self textField:textField setLength:maxLength];
    
    tmpStringLength = textField.text.length;
    
    label.text = [NSString stringWithFormat:@"You have %li characters left", (maxLength - tmpStringLength)];
    
}

- (NSInteger) countLittleCyanLabel {
    
    NSInteger labelCountWithCyanText = 0;
    
    for (int i = 0; i < [self.littleLabelCollection count]; i++) {
        
        UILabel *currentLittleLabel = [self.littleLabelCollection objectAtIndex:i];
        
        if ([currentLittleLabel.textColor isEqual:[UIColor cyanColor]]) {
            labelCountWithCyanText++;
        }
    }
    
    return labelCountWithCyanText;
}

- (void) nextFieldForChangeFromIndex:(NSInteger)textFieldIndex {
    
    NSInteger newTextFieldIndex = [self.littleLabelCollection indexOfObject:[self.littleLabelCollection objectAtIndex:textFieldIndex]];
    
    [[self.informationTextFieldCollection objectAtIndex:newTextFieldIndex] becomeFirstResponder];
}

- (void) setPhoneNumberFormatForTextField:(UITextField *)currentTextField {
    
    NSMutableString* resultString = [NSMutableString string];
    
    NSInteger localNumberLength = MIN([currentTextField.text length], localNumberMaxLength);
    
    if (localNumberLength > 0) {
        
        NSString* number = [currentTextField.text substringFromIndex:(int)[currentTextField.text length] - localNumberLength];
        
        [resultString appendString:number];
        
        if ([resultString length] > 3) {
            [resultString insertString:@"-" atIndex:3];
        }
    }
    
    if ([currentTextField.text length] > localNumberMaxLength) {
        
        NSInteger areaCodeLength = MIN((int)[currentTextField.text length] - localNumberMaxLength, areaCodeMaxLength);
        
        NSRange areaRange = NSMakeRange((int)[currentTextField.text length] - localNumberMaxLength - areaCodeLength, areaCodeLength);
        
        NSString* area = [currentTextField.text substringWithRange:areaRange];
        
        area = [NSString stringWithFormat:@"(%@) ", area];
        
        [resultString insertString:area atIndex:0];
    }
    
    if ([currentTextField.text length] > localNumberMaxLength + areaCodeMaxLength) {
        
        NSInteger countryCodeLength = MIN((int)[currentTextField.text length] - localNumberMaxLength - areaCodeMaxLength, countryCodeMaxLength);
        
        NSRange countryCodeRange = NSMakeRange(0, countryCodeLength);
        
        NSString* countryCode = [currentTextField.text substringWithRange:countryCodeRange];
        
        countryCode = [NSString stringWithFormat:@"+%@ ", countryCode];
        
        [resultString insertString:countryCode atIndex:0];
    }
    
    currentTextField.text = resultString;
}

- (void) saveRegistrationData {
    
    NSError *error1 = nil;
    
    NSString *path = @"/Users/alexanderberezovskyy/Documents/Курсы_iOS/Скутаренко_ДЗ/For_Show/Video&Music_with_registration_form/Registration_data/reg_data.txt";
    
    NSString *content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUTF8StringEncoding
                                                     error:&error1];
    
    content = [content stringByAppendingString:self.registrationData];
    
    NSError *error2 = nil;
    
    Boolean succeed = [content writeToFile:path
                                atomically:YES
                                  encoding:NSUTF8StringEncoding
                                     error:&error2];
    
    if (!succeed){
        NSLog(@"Can't saved new person");
    }
}

- (void) clearAll {
    
    for (UITextField *textField in self.informationTextFieldCollection) {
        
        textField.text = @"";
    }
    
    for (UILabel *label in self.littleLabelCollection) {
        
        label.text = @"";
        label.textColor = [UIColor whiteColor];
    }
    
    [self.submitButton setEnabled:NO];
    [self.rememberSwitch setOn:NO];
    
    if (![self.privateInfo isEqualToString:@""]) {
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        [userDefaults setInteger:0              forKey:@"genderKey"];
        [userDefaults setInteger:0              forKey:@"countryKey"];
        [userDefaults setObject:[NSDate date]   forKey:@"birthdayKey"];
        [userDefaults setObject:@""             forKey:@"privateInfoAll"];
        
        [userDefaults synchronize];
    }
}


#pragma mark - Borders & Colors

- (void) view:(UIView *)currentView setCornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor shadowOpacity:(CGFloat)shadowOpacity shadowColor:(UIColor *)shadowColor tintColor:(UIColor *)tintColor {
    
    currentView.layer.cornerRadius = cornerRadius;
    currentView.layer.borderWidth = borderWidth;
    currentView.layer.borderColor = borderColor.CGColor;
    currentView.layer.shadowOpacity = shadowOpacity;
    currentView.layer.shadowColor = shadowColor.CGColor;
    currentView.tintColor = tintColor;
    
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"cell_4.jpg"]];
    
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return NO;
}


#pragma mark - Remember and Load

- (void) saveRegistrationTextFieldData {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:self.loginTextField.text        forKey:kRegistrationLogin];
    [userDefaults setObject:self.passwordTextField.text     forKey:kRegistrationPassword];
    [userDefaults setObject:self.firstNameTextField.text    forKey:kRegistrationFirstName];
    [userDefaults setObject:self.lastNameTextField.text     forKey:kRegistrationLastName];
    [userDefaults setObject:self.ageTextField.text          forKey:kRegistrationAge];
    [userDefaults setObject:self.emailTextField.text        forKey:kRegistrationEmail];
    [userDefaults setObject:self.phoneTextField.text        forKey:kRegistrationPhone];
    
    [userDefaults setObject:self.stringAfterAt  forKey:kEmailPartAfterAt];
    
    [userDefaults synchronize];
}

- (void) loadRegistrationTextFieldData {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.loginTextField.text = [userDefaults objectForKey:kRegistrationLogin];
    self.passwordTextField.text = [userDefaults objectForKey:kRegistrationPassword];
    self.firstNameTextField.text = [userDefaults objectForKey:kRegistrationFirstName];
    self.lastNameTextField.text = [userDefaults objectForKey:kRegistrationLastName];
    self.ageTextField.text = [userDefaults objectForKey:kRegistrationAge];
    self.emailTextField.text = [userDefaults objectForKey:kRegistrationEmail];
    self.phoneTextField.text = [userDefaults objectForKey:kRegistrationPhone];
    

    
    for (UILabel *littleLabel in self.littleLabelCollection) {
        
        littleLabel.textColor = [UIColor cyanColor];
        littleLabel.text = @"Success!";
    }
    
    self.stringAfterAt = [userDefaults objectForKey:kEmailPartAfterAt];
    
    [self.submitButton setEnabled:YES];
    
}

- (void) saveStateRememberSwitch {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setBool:self.rememberSwitch.isOn forKey:kRegistrationRemember];
    
    [userDefaults synchronize];
}

- (void) loadStateRememberSwitch {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.rememberSwitch.on = [userDefaults boolForKey:kRegistrationRemember];
    
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"showSuccessControllerFromTableView"]){
        
        ABSuccessRegistrationVC *successVC = (ABSuccessRegistrationVC *)segue.destinationViewController;
        successVC.firstAndLastNames = [NSString stringWithFormat:@"Registration was successfull!\n \nHello %@ %@!", self.firstNameTextField.text, self.lastNameTextField.text];
    }
}


@end
