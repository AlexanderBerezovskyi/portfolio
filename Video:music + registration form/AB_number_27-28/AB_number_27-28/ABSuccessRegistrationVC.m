//
//  SuccessRegistrationVC.m
//  AB_number_27-28
//
//  Created by Alexander Berezovskyy on 11.04.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABSuccessRegistrationVC.h"

@interface ABSuccessRegistrationVC ()

@property (weak, nonatomic) IBOutlet UILabel *successRegistrationLabel;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *audioVideoButtons;

@end

@implementation ABSuccessRegistrationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // buttons settings
    
    for (UIButton *button in self.audioVideoButtons) {
        
        button.layer.borderWidth = 2.f;
        button.layer.borderColor = [UIColor whiteColor].CGColor;

    }
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_2.jpg"]];
    
    self.successRegistrationLabel.text = self.firstAndLastNames;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
