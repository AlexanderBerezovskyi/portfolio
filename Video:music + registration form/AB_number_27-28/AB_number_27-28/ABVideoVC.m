//
//  ABVideoVC.m
//  AB_number_27-28
//
//  Created by Alexander Berezovskyy on 18.04.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABVideoVC.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <Photos/Photos.h>

@interface ABVideoVC () <UIPickerViewDelegate, UIPickerViewDataSource>

// video

@property (nonatomic, strong) AVPlayer *playerVideo;
@property (nonatomic, strong) NSMutableArray *videoFilesArray;
@property (nonatomic, strong) PHFetchResult *videosAssetsArray;
@property (nonatomic, weak) PHAsset *currentAsset;

@property (nonatomic, strong) NSString *selectedRowVideoPV;

@property (weak, nonatomic) IBOutlet UIPickerView *selectVideoPickerView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@end

@implementation ABVideoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // get array of videos on real device
    
    self.videosAssetsArray = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeVideo options:nil];
    
    // settings play button and picker view
    
    self.playButton.layer.borderWidth = self.selectVideoPickerView.layer.borderWidth = 2.f;
    self.playButton.layer.borderColor = self.selectVideoPickerView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cell_4.jpg"]];
    
 
    // create array with objects

    self.videoFilesArray = [[NSMutableArray alloc] init];

    for (PHAsset *video in self.videosAssetsArray) {
        NSDictionary *dictVideo =
        [NSDictionary dictionaryWithObjectsAndKeys:
         video.creationDate, @"creationDate",
         [video valueForKey:@"filename"], @"videoName", nil];
        
        [self.videoFilesArray addObject:dictVideo];
    }
    
    if (self.videoFilesArray.count != 0) {
        self.currentAsset = [self.videosAssetsArray objectAtIndex:0];
    }
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
//    self.currentAsset = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Actions

- (IBAction)videoAction:(UIButton *)sender {
    
    [[PHImageManager defaultManager]
     requestPlayerItemForVideo:self.currentAsset
     options:nil
     resultHandler:^(AVPlayerItem * _Nullable playerItem, NSDictionary * _Nullable info) {
         self.playerVideo = [AVPlayer playerWithPlayerItem:playerItem];
     }];
    
    // create a player view controller
    
    AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
    [self presentViewController:controller animated:YES completion:nil];
    controller.player = self.playerVideo;
    [self.playerVideo play];
}


#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.videoFilesArray.count;
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.currentAsset = [self.videosAssetsArray objectAtIndex:row];
}

- (nullable NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSDate *creationDate = [[self.videoFilesArray objectAtIndex:row] valueForKey:@"creationDate"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy.dd.MM HH:mm"];
    
    NSString *tmpString = [NSString stringWithFormat:@"%@ %@", [[self.videoFilesArray objectAtIndex:row] valueForKey:@"videoName"], [dateFormatter stringFromDate:creationDate]];
    
    NSAttributedString *tmpAttributedString = [[NSAttributedString alloc] initWithString:tmpString attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],               NSFontAttributeName:[UIFont fontWithName:@"Papyrus-Condensed" size:10.f]}];
    
    return tmpAttributedString;
}


@end
