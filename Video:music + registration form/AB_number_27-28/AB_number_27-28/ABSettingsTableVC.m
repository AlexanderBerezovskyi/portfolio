//
//  ABSettingsTableVC.m
//  AB_number_27-28
//
//  Created by Alexander Berezovskyy on 21.04.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABSettingsTableVC.h"

static NSString *kSettingsGender         = @"genderKey";
static NSString *kSettingsBirthday       = @"birthdayKey";
static NSString *kSettingsCountry        = @"countryKey";
static NSString *kSettingsAllPrivateInfo = @"privateInfoAll";

typedef enum {
    
    ABGenderUnspecified,
    ABGenderWoman,
    ABGenderMan
    
}ABGender;

@interface ABSettingsTableVC () <UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate>

// outlets

@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegmentedControl;
@property (weak, nonatomic) IBOutlet UIDatePicker *birthdayDatePickerView;
@property (weak, nonatomic) IBOutlet UIPickerView *countryPickerView;

// private info data

@property (strong, nonatomic) NSString *allPrivateInfo;
@property (strong, nonatomic) NSString *gender;
@property (strong, nonatomic) NSString *dateOfBirth;
@property (strong, nonatomic) NSString *currentCountry;

@property (strong, nonatomic) NSMutableArray *countriesArray;

@property (assign, nonatomic) NSInteger selectedRowCountryPickerView;

@end

@implementation ABSettingsTableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Date Picker View & Country Picker View

    self.countryPickerView.layer.borderWidth = self.birthdayDatePickerView.layer.borderWidth = 0.3f;
    self.countryPickerView.layer.borderColor = self.birthdayDatePickerView.layer.borderColor = [UIColor brownColor].CGColor;
    
    [self.birthdayDatePickerView setValue:[UIColor brownColor] forKey:@"textColor"];
    
    self.birthdayDatePickerView.maximumDate = [NSDate date];
    
    // Countries Array
    
    self.countriesArray = [NSMutableArray new];
    
    NSLocale *locale = [NSLocale autoupdatingCurrentLocale];
    NSArray *countryTextCodesArray = [NSLocale ISOCountryCodes];

    for (NSString *countryCode in countryTextCodesArray) {
        
        NSString *displayNameString = [locale displayNameForKey:NSLocaleCountryCode
                                                          value:countryCode];
        
        [self.countriesArray addObject:displayNameString];
    }
    
    [self.countriesArray sortUsingSelector:@selector(localizedCompare:)];
    [self.countriesArray insertObject:@"not selected" atIndex:0];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadSettings];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return self.countriesArray.count;
}

#pragma mark - UIPickerViewDelegate

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    
    return 40.f;
}

- (nullable NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *tmpString = [NSString stringWithFormat:@"%@", [self.countriesArray objectAtIndex:row]];
    
    NSAttributedString *tmpAttributedString = [[NSAttributedString alloc] initWithString:tmpString attributes:@{NSForegroundColorAttributeName:[UIColor brownColor],               NSFontAttributeName:[UIFont fontWithName:@"Arial" size:8.f], NSUnderlineStyleAttributeName:@(NSUnderlineStyleDouble)}];
    
    return tmpAttributedString;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.currentCountry = [NSString stringWithFormat:@"%@", [self.countriesArray objectAtIndex:row]];
    self.selectedRowCountryPickerView = [pickerView selectedRowInComponent:0];
}

#pragma mark - UITableViewDelegate

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return NO;
}

#pragma mark - Save & Load

- (void) saveSettings {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setInteger:self.genderSegmentedControl.selectedSegmentIndex forKey:kSettingsGender];
    [userDefaults setInteger:self.selectedRowCountryPickerView forKey:kSettingsCountry];
    [userDefaults setObject:self.birthdayDatePickerView.date forKey:kSettingsBirthday];
    
    // Save Private Info in string format
    
    // Country
    self.currentCountry = [self.countriesArray objectAtIndex:[userDefaults integerForKey:kSettingsCountry]];
    
    // Birthday
    NSDateFormatter *birthDayFormat = [[NSDateFormatter alloc] init];
    [birthDayFormat setDateFormat:@"MM d yyyy"];
    self.dateOfBirth = [birthDayFormat stringFromDate:self.birthdayDatePickerView.date];
    
    // Gender
    switch (self.genderSegmentedControl.selectedSegmentIndex) {
        case ABGenderUnspecified:
            self.gender = @"Unspecified";
            break;
            
        case ABGenderWoman:
            self.gender = @"Woman";
            break;
            
        case ABGenderMan:
            self.gender = @"Man";
            break;
            
        default:
            break;
    }
    
    self.allPrivateInfo = [NSString stringWithFormat:@"\nPrivate info:\ngender = %@,\nbirthday = %@,\ncountry = %@\n", self.gender, self.dateOfBirth, self.currentCountry];
    
    [userDefaults setObject:self.allPrivateInfo forKey:kSettingsAllPrivateInfo];
    
    [userDefaults synchronize];
}

- (void) loadSettings {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.genderSegmentedControl.selectedSegmentIndex = [userDefaults integerForKey:kSettingsGender];
    [self.birthdayDatePickerView setDate:[userDefaults objectForKey:kSettingsBirthday] animated:YES];
    [self.countryPickerView selectRow:[userDefaults integerForKey:kSettingsCountry] inComponent:0 animated:YES];
}


#pragma mark - Actions

- (IBAction)actionSaveSettingsButton:(UIButton *)sender {
    [self saveSettings];
}


@end
