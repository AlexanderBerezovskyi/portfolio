//
//  AppDelegate.h
//  ABFonts
//
//  Created by Alexander Berezovskyy on 25.04.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

