//
//  ABFontTableVC.m
//  ABFonts
//
//  Created by Alexander Berezovskyy on 25.04.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABFontTableVC.h"

@interface ABFontTableVC () <UITableViewDataSource>

@property (strong, nonatomic) NSArray *fontsArray;

@end

@implementation ABFontTableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIEdgeInsets insets = UIEdgeInsetsMake(40, 0, 0, 0);
    self.tableView.contentInset = insets;
    self.tableView.scrollIndicatorInsets = insets;
    
    self.fontsArray = [UIFont familyNames];
    self.fontsArray = [self.fontsArray sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return self.fontsArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    NSArray *currentFontFamilyArray = [UIFont fontNamesForFamilyName:[self.fontsArray objectAtIndex:section]];
    
    return currentFontFamilyArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = @"CellFontIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    NSArray *currentFontFamilyArray = [UIFont fontNamesForFamilyName:[self.fontsArray objectAtIndex:indexPath.section]];
    
    NSString* currentFont = [currentFontFamilyArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = currentFont;
    cell.textLabel.font = [UIFont fontWithName:currentFont size:17.f];
    
    return cell;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *titleFont = [self.fontsArray objectAtIndex:section];
    return titleFont;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *) view;
    
    header.textLabel.textColor = [UIColor blueColor];
    header.textLabel.font = [UIFont fontWithName:@"Arial" size:17.f];
    header.textLabel.textAlignment = NSTextAlignmentCenter;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20.f;
}


@end
