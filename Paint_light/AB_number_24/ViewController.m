//
//  ViewController.m
//  AB_number_24
//
//  Created by Alexander Berezovskyy on 11.03.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ViewController.h"
#import "MBProgressHUD.h"

typedef enum {
    
    ABColorGreen = 1,
    ABColorYellow,
    ABColorPurple,
    ABColorRed,
    ABColorBlue,
    ABColorBrown,
    ABColorMagenta,
    ABColorCyan,
    ABColorWhite,
    ABColorBlack
    
} ABColor;

typedef enum {
    
    ABLineSize1 = 11,
    ABLineSize2,
    ABLineSize3,
    ABLineSize4,
    ABLineSize5
    
}ABLineSize;

@interface ViewController ()

@property (nonatomic, assign) CGPoint lastPoint;
@property (nonatomic,weak) IBOutlet UIImageView *drawImage;

@property (nonatomic, strong) UIColor *pencilColor;
@property (nonatomic, assign) CGFloat pencilSize;
@property (nonatomic, assign) CGFloat rectMinSide;
@property (nonatomic, assign) CGFloat timeProgress;

@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, strong) NSTimer *timerForProgress;

// Other buttons
- (IBAction)clearAllActionButton:(UIButton *)sender;
- (IBAction)saveImageActionButton:(UIButton *)sender;
- (IBAction)changeColorAction:(UIButton *)sender;
- (IBAction)changeLineSizeAction:(UIButton *)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGFloat rectMinSide = MIN(CGRectGetWidth(self.drawImage.bounds),
                              CGRectGetHeight(self.drawImage.bounds));
    
    self.rectMinSide = rectMinSide;
    
    // Color
    self.pencilColor = [UIColor blackColor];
    
    // Font Size
    self.pencilSize = rectMinSide/100;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITouch

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    self.lastPoint = [touch locationInView:self.view];
    
    [self drawInTouch:touch];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    
    [self drawInTouch:touch];
}

#pragma mark - Draw in Touch

- (void) drawInTouch:(UITouch *)currentTouch {
    
    CGPoint currentPoint = [currentTouch locationInView:self.view];
    
    // Create rect for painting
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    CGRect drawRect = CGRectMake(0, 0,
                                 self.view.frame.size.width,
                                 self.view.frame.size.height);
    
    [self.drawImage.image drawInRect:drawRect];
    
    // Settings for Pencil
    
    CGContextSetLineCap              (UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth            (UIGraphicsGetCurrentContext(), self.pencilSize);
    CGContextSetStrokeColorWithColor (UIGraphicsGetCurrentContext(), self.pencilColor.CGColor);
    
    // Create lines between points
    
    CGContextBeginPath      (UIGraphicsGetCurrentContext());
    CGContextMoveToPoint    (UIGraphicsGetCurrentContext(), self.lastPoint.x, self.lastPoint.y);
    CGContextAddLineToPoint (UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
    CGContextStrokePath     (UIGraphicsGetCurrentContext());
    
    self.drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.lastPoint = currentPoint;
}

#pragma mark - Actions

// Colors

- (IBAction)changeColorAction:(UIButton *)sender {
    
    switch (sender.tag) {
            
        case ABColorGreen:
            self.pencilColor = [UIColor greenColor];
            break;
            
        case ABColorYellow:
            self.pencilColor = [UIColor yellowColor];
            break;
            
        case ABColorPurple:
            self.pencilColor = [UIColor purpleColor];
            break;
            
        case ABColorRed:
            self.pencilColor = [UIColor redColor];
            break;
            
        case ABColorBlue:
            self.pencilColor = [UIColor blueColor];
            break;
            
        case ABColorBrown:
            self.pencilColor = [UIColor brownColor];
            break;
            
        case ABColorMagenta:
            self.pencilColor = [UIColor magentaColor];
            break;
            
        case ABColorCyan:
            self.pencilColor = [UIColor cyanColor];
            break;
            
        case ABColorWhite:
            self.pencilColor = [UIColor whiteColor];
            break;
            
        case ABColorBlack:
            self.pencilColor = [UIColor blackColor];
            break;
            
        default:
            break;
    }
}

// Line sizes

- (IBAction)changeLineSizeAction:(UIButton *)sender {
    
    switch (sender.tag) {
            
        case ABLineSize1:
            self.pencilSize = self.rectMinSide/100;
            break;
            
        case ABLineSize2:
            self.pencilSize = self.rectMinSide/50;
            break;
            
        case ABLineSize3:
            self.pencilSize = self.rectMinSide/26;
            break;
            
        case ABLineSize4:
            self.pencilSize = self.rectMinSide/15;
            break;
            
        case ABLineSize5:
            self.pencilSize = self.rectMinSide/8;
            break;
            
        default:
            break;
    }
}

- (IBAction)clearAllActionButton:(UIButton *)sender {
    self.drawImage.image = nil;
    self.drawImage.image = [UIImage imageNamed:@"papirus.jpg"];
}

- (IBAction)saveImageActionButton:(UIButton *)sender {

    UIGraphicsBeginImageContext(self.drawImage.bounds.size);
    [self.drawImage.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    
    // save in photo album on real device
    
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    
    self.timeProgress = 0.f;
    
    self.hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
    
    self.timerForProgress = [NSTimer
                             scheduledTimerWithTimeInterval:0.05f
                             target:self
                             selector:@selector(progressByTimer:)
                             userInfo:nil
                             repeats:YES];
    
    [self.timerForProgress fire];
}

- (void) progressByTimer:(NSTimer *)timer {
    
    self.timeProgress = self.timeProgress + 0.05f;
    
    self.hud.progress = self.timeProgress / 1;
    
    if (self.hud.progress == 1) {
        [self.timerForProgress invalidate];
        [self.hud hideAnimated:YES];
    }
}



@end
