//
//  ABSaveUserTVC.h
//  AB_number_41-44
//
//  Created by Alexander Berezovskyy on 16.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABUser;

@interface ABSaveUserTVC : UITableViewController

@property (strong, nonatomic) ABUser *user;

@end
