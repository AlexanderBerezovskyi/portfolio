//
//  AppDelegate.h
//  AB_number_41-44
//
//  Created by Alexander Berezovskyy on 15.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

