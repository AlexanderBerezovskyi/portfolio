//
//  ABDataManager.h
//  AB_number_41-44
//
//  Created by Alexander Berezovskyy on 15.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface ABDataManager : NSObject

@property (readonly, strong) NSPersistentContainer *persistentContainer;

+ (ABDataManager *) sharedManager;
- (void)saveContext;

@end
