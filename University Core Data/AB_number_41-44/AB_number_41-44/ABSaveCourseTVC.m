//
//  ABSaveCourseTVC.m
//  AB_number_41-44
//
//  Created by Alexander Berezovskyy on 20.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABSaveCourseTVC.h"
#import "ABCourseTVCell.h"
#import "ABCourse+CoreDataProperties.h"
#import "ABDataManager.h"
#import "ABUser+CoreDataProperties.h"
#import "ABSaveUserTVC.h"
#import "ABCheckmarkTVC.h"

// segues
static NSString *checkTeacherSegue  = @"checkTeacher";
static NSString *checkStudentsSegue = @"checkStudents";
static NSString *editUser2Segue     = @"editUser2";

// first section of table view
static NSString* courseNameIdentifier = @"CourseNameCell";
static NSString* subjectIdentifier    = @"SubjectCell";
static NSString* teacherIdentifier    = @"TeacherCell";

// second section of table view
static NSString* addUserInCourseIdentifier = @"AddUserCell";
static NSString* showUserIdentifier        = @"ShowUserCell";

typedef enum {
   
    ABCellCourseNameCourse,
    ABCellCourseSubject,
    ABCellCourseTeacher
    
} ABCellCourse;

typedef enum {
    
    ABSectionCourseInfo,
    ABSectionCourseUsers
    
} ABSectionCourse;

typedef enum {
    
    ABCellCourseUserAdd,
    ABCellCourseUserShow
    
} ABCellCourseUser;

@interface ABSaveCourseTVC () <UITextFieldDelegate, UITableViewDataSource>

@property (strong, nonatomic) ABCourseTVCell *infoCourseCell;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;

@end

@implementation ABSaveCourseTVC

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.infoCourseCell = [[ABCourseTVCell alloc] init];
    
    [self saveButtonEnabled];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1.f;
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    
    if ((indexPath.section == ABSectionCourseUsers) && (indexPath.row > 0)) {
        
        ABUser *user = [[self sortedUsersArrayByLastFirstName] objectAtIndex:indexPath.row - 1];
        
        self.user = user;
        
        [self performSegueWithIdentifier:editUser2Segue sender:nil];
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == ABSectionCourseInfo) {
        return 3;
    } else {
        return (1 + self.course.users.count);
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ABCourseTVCell *courseCell = nil;
    
    if (indexPath.section == ABSectionCourseInfo) {
    
        switch (indexPath.row) {
                
            case ABCellCourseNameCourse:
                
                courseCell = [tableView dequeueReusableCellWithIdentifier:courseNameIdentifier
                                                           forIndexPath:indexPath];
                self.infoCourseCell.courseNameTextField = courseCell.courseNameTextField;
                
                if (self.courseAction == ABCourseActionEdit) {
                    courseCell.courseNameTextField.text = self.course.name;
                }

                break;
                
            case ABCellCourseSubject:
                
                courseCell = [tableView dequeueReusableCellWithIdentifier:subjectIdentifier
                                                           forIndexPath:indexPath];
                self.infoCourseCell.subjectTextField = courseCell.subjectTextField;
                
                if (self.courseAction == ABCourseActionEdit) {
                courseCell.subjectTextField.text = self.course.subject;
                }
                
                break;
                
            case ABCellCourseTeacher:
                
                courseCell = [tableView dequeueReusableCellWithIdentifier:teacherIdentifier
                                                           forIndexPath:indexPath];
                
                if (self.course) {
                    courseCell.teacherTextField.text = [NSString stringWithFormat:@"%@ %@", self.course.teacher.lastName, self.course.teacher.firstName];
                    
                } else {
                    
                    if (self.user) {
                        courseCell.teacherTextField.text = [NSString stringWithFormat:@"%@ %@", self.user.lastName, self.user.firstName];
                    }
                }
                
                self.infoCourseCell.teacherTextField = courseCell.teacherTextField;
                
                break;
                
            default:
                break;
        }
        
    } else if (indexPath.section == ABSectionCourseUsers) {
        
        if (indexPath.row == ABCellCourseUserAdd) {
            courseCell = [tableView dequeueReusableCellWithIdentifier:addUserInCourseIdentifier
                                                         forIndexPath:indexPath];
            if (!self.course) {
                [courseCell.addRemoveInCourseButton setEnabled:NO];
            }
            
        } else {
            
            courseCell = [tableView dequeueReusableCellWithIdentifier:showUserIdentifier
                                                         forIndexPath:indexPath];
            
            if (!courseCell) {
                courseCell = [[ABCourseTVCell alloc] initWithStyle:UITableViewCellStyleValue1
                                                   reuseIdentifier:showUserIdentifier];
            }
            
            ABUser *user = [[self sortedUsersArrayByLastFirstName] objectAtIndex:indexPath.row - 1];
            
            courseCell.textLabel.text = [NSString stringWithFormat:@"%@ %@", user.lastName, user.firstName];
            courseCell.detailTextLabel.text = [NSString stringWithFormat:@"%@", user.email];
            courseCell.detailTextLabel.textColor = [UIColor blueColor];
            courseCell.imageView.image = [UIImage imageWithData:user.photo];;
        }
    }
    
    [self saveButtonEnabled];
    
    return courseCell;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == ABSectionCourseInfo) {
        return @"Course info";
    } else {
        return @"Users in course";
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ((indexPath.section == ABSectionCourseUsers) && (indexPath.row > 0)) {
        return YES;
    } else {
        return NO;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ((editingStyle == UITableViewCellEditingStyleDelete) && (indexPath.section == ABSectionCourseUsers)) {

        ABUser *user = [[self sortedUsersArrayByLastFirstName] objectAtIndex:indexPath.row - 1];
        
        [self.course removeUsersObject:user];
        
        [[ABDataManager sharedManager] saveContext];
        
        [self.tableView beginUpdates];
        
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        
        [self.tableView endUpdates];
    }
}


#pragma mark - Actions

- (IBAction)saveAction:(UIBarButtonItem *)sender {
    
    if (self.course) {
        
        self.course.name    = self.infoCourseCell.courseNameTextField.text;
        self.course.subject = self.infoCourseCell.subjectTextField.text;
    
    } else {
    
        ABCourse *course = [NSEntityDescription insertNewObjectForEntityForName:@"ABCourse" inManagedObjectContext:[ABDataManager sharedManager].persistentContainer.viewContext];
        
        course.name    = self.infoCourseCell.courseNameTextField.text;
        course.subject = self.infoCourseCell.subjectTextField.text;
        if (self.user) {
            course.teacher = self.user;
        }
    }
    
    [[ABDataManager sharedManager] saveContext];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)changeCourseInfoAction:(UITextField *)sender {
    [self saveButtonEnabled];
}

- (IBAction)setTeacherAction:(UITextField *)sender {

    [self performSegueWithIdentifier:checkTeacherSegue sender:nil];
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField isEqual:self.infoCourseCell.courseNameTextField]) {
        [self.infoCourseCell.subjectTextField becomeFirstResponder];
        
    } else if ([textField isEqual:self.infoCourseCell.subjectTextField]) {
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self saveButtonEnabled];
}


#pragma mark - Reduce Methods

- (void) saveButtonEnabled {
    
    if (self.infoCourseCell.courseNameTextField.text &&
        self.infoCourseCell.subjectTextField.text &&
        self.infoCourseCell.teacherTextField.text &&
        ![self.infoCourseCell.courseNameTextField.text isEqualToString:@""] &&
        ![self.infoCourseCell.subjectTextField.text isEqualToString:@""] &&
        ![self.infoCourseCell.teacherTextField.text isEqualToString:@""]) {
        
        [self.saveButton setEnabled:YES];
        
    } else {
        [self.saveButton setEnabled:NO];
    }
}

- (NSMutableArray *) sortedUsersArrayByLastFirstName {
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:[self.course.users allObjects]];
    
    NSSortDescriptor *sortDescriptor1 =
    [NSSortDescriptor sortDescriptorWithKey:@"firstName" ascending:YES];
    
    NSSortDescriptor *sortDescriptor2 =
    [NSSortDescriptor sortDescriptorWithKey:@"lastName" ascending:YES];
    
    [array sortUsingDescriptors:@[sortDescriptor2, sortDescriptor1]];
    
    return array;
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:editUser2Segue]){
        
        ABSaveUserTVC *saveUserVC = (ABSaveUserTVC *)segue.destinationViewController;
        saveUserVC.user = self.user;
    }
    
    if ([segue.identifier isEqualToString:checkStudentsSegue]){
        
        ABCheckmarkTVC *checkmarkTVC = (ABCheckmarkTVC *)segue.destinationViewController;
        checkmarkTVC.course = self.course;
        checkmarkTVC.checkmarkType = ABCheckmarkSelectStudents;
    }
    
    if ([segue.identifier isEqualToString:checkTeacherSegue]){
        
        ABCheckmarkTVC *checkmarkTVC = (ABCheckmarkTVC *)segue.destinationViewController;
        
        if (self.course) {
            checkmarkTVC.checkmarkType = ABCheckmarkSelectTeacherForCurrentCourse;
            checkmarkTVC.course = self.course;
        
        } else {
            
            checkmarkTVC.checkmarkType = ABCheckmarkSelectTeacherForNewCourse;
            
            checkmarkTVC.callBackUser = ^(ABUser *userBack) {
                self.user = userBack;
            };
        }
    }
}


@end
