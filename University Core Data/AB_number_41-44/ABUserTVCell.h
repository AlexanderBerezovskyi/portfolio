//
//  ABUserTVCell.h
//  AB_number_41-44
//
//  Created by Alexander Berezovskyy on 19.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABUserTVCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@end
