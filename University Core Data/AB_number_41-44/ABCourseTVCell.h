//
//  ABCourseTVCell.h
//  AB_number_41-44
//
//  Created by Alexander Berezovskyy on 20.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABCourseTVCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *courseNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *subjectTextField;
@property (weak, nonatomic) IBOutlet UITextField *teacherTextField;

@property (weak, nonatomic) IBOutlet UIButton *addRemoveInCourseButton;

@end
